<?php

declare(strict_types=1);

// ===== convert errors to exceptions =====

set_error_handler(
    function ($severity, $message, $file, $line): bool {
        // $severity should not be reported according to error_reporting()
        if (($severity & error_reporting()) === 0) {
            return true;
        }
        $vendorDir = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR;
        // ignore minor severity engine messages caused by 3rd party code
        $severityToIgnore = E_DEPRECATED | E_NOTICE;
        if (PHP_VERSION_ID < 80400) {
            $severityToIgnore = $severityToIgnore | E_STRICT;
        }
        if (($severity & $severityToIgnore) !== 0 && strpos($file, $vendorDir) === 0) {
            return true;
        }
        // ignore minor severity user messages caused by 3rd party code
        if (($severity & (E_USER_DEPRECATED | E_USER_NOTICE)) !== 0 && strpos($file, $vendorDir) === 0) {
            // top of the $callStack is the call to the error handler (this function) ...
            $callStack = debug_backtrace();
            // ... next() is the call to trigger_error() ...
            next($callStack);
            // ... and next() after that should be the $caller that actually caused the error
            $caller = next($callStack);
            if ($caller !== false && strpos($caller['file'] ?? '', $vendorDir) === 0) {
                return true;
            }
        }
        // convert message to exception
        throw new ErrorException($message, 0, $severity, $file, $line);
    },
);

// ===== initialize autoloader =====

require dirname(__DIR__) . '/vendor/autoload.php';

// ===== define test data providers =====

\BjoernGoetschke\Test\EventStore\TestHelper::registerSqliteEventStoreDataProvider(
    'sqlite_memory',
    'sqlite::memory:',
    null,
    null,
);

\BjoernGoetschke\Test\EventStore\TestHelper::registerMySQLEventStoreDataProvider(
    'mysql',
    'mysql:host=mariadb-test;port=3306;dbname=' . $_SERVER['MARIADB_TEST_DATABASE'],
    $_SERVER['MARIADB_TEST_USER'],
    $_SERVER['MARIADB_TEST_PASSWORD'],
);
