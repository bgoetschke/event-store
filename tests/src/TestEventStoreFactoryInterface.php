<?php

declare(strict_types=1);

namespace BjoernGoetschke\Test\EventStore;

use BjoernGoetschke\EventStore\EventStoreInterface;

interface TestEventStoreFactoryInterface
{
    public function build(): EventStoreInterface;
}
