<?php

declare(strict_types=1);

namespace BjoernGoetschke\Test\EventStore\Acceptance;

use BjoernGoetschke\EventStore\Event\EventNumber;
use BjoernGoetschke\EventStore\Event\EventType;
use BjoernGoetschke\EventStore\Event\StreamEvent;
use BjoernGoetschke\EventStore\StreamUid;
use BjoernGoetschke\Test\EventStore\TestEventStoreFactoryInterface;
use BjoernGoetschke\Test\EventStore\TestHelper;
use PHPUnit\Framework\TestCase;

final class BasicUsageTest extends TestCase
{
    /**
     * @return array<string, array<TestEventStoreFactoryInterface>>
     */
    public function dataProvider(): array
    {
        return TestHelper::eventStoreDataProvider();
    }

    /**
     * @dataProvider dataProvider
     */
    public function testBasicUsage(TestEventStoreFactoryInterface $factory): void
    {
        $eventStore = $factory->build();

        TestHelper::assertUidStreamMatches(
            $eventStore->streams(),
            [],
        );

        self::assertNull(
            $eventStore->position(new StreamUid('SomeNotExistingUid')),
        );

        TestHelper::assertEventStreamMatches(
            $eventStore->streamEvents(new StreamUid('SomeNotExistingUid')),
            [],
        );

        TestHelper::assertEventStreamMatches(
            $eventStore->streamEvents(new StreamUid('SomeNotExistingUid'), new EventNumber(1)),
            [],
        );

        $eventStore->write(
            [
                StreamEvent::fromBasicTypes('Stream1', 1, 'Type1', 'Data1'),
                StreamEvent::fromBasicTypes('Stream1', 2, 'Type2', 'Data2'),
                StreamEvent::fromBasicTypes('Stream1', 3, 'Type3', 'Data3'),
            ],
        );

        self::assertNotNull(
            $eventStore->position(new StreamUid('Stream1')),
        );

        self::assertSame(
            3,
            $eventStore->position(new StreamUid('Stream1'))->toInt(),
        );

        TestHelper::assertEventStreamMatches(
            $eventStore->allEvents(),
            [
                ['Stream1', 1, 'Type1', 'Data1'],
                ['Stream1', 2, 'Type2', 'Data2'],
                ['Stream1', 3, 'Type3', 'Data3'],
            ],
        );

        TestHelper::assertEventStreamMatches(
            $eventStore->streamEvents(new StreamUid('Stream1')),
            [
                ['Stream1', 1, 'Type1', 'Data1'],
                ['Stream1', 2, 'Type2', 'Data2'],
                ['Stream1', 3, 'Type3', 'Data3'],
            ],
        );

        TestHelper::assertEventStreamMatches(
            $eventStore->streamEvents(new StreamUid('Stream1'), new EventNumber(2)),
            [
                ['Stream1', 3, 'Type3', 'Data3'],
            ],
        );

        TestHelper::assertEventStreamMatches(
            $eventStore->streamEvents(new StreamUid('Stream1'), new EventNumber(3)),
            [],
        );

        TestHelper::assertEventStreamMatches(
            $eventStore->streamEvents(new StreamUid('Stream1'), new EventNumber(4)),
            [],
        );

        TestHelper::assertUidStreamMatches(
            $eventStore->streams(),
            [
                'Stream1',
            ],
        );
    }

    /**
     * @dataProvider dataProvider
     */
    public function testEmptyWritesBeforeActualWrites(TestEventStoreFactoryInterface $factory): void
    {
        $eventStore = $factory->build();

        self::assertNull(
            $eventStore->position(new StreamUid('Stream1')),
        );

        TestHelper::assertEventStreamMatches(
            $eventStore->streamEvents(new StreamUid('Stream1')),
            [],
        );

        $eventStore->write([]);

        self::assertNull(
            $eventStore->position(new StreamUid('Stream1')),
        );

        TestHelper::assertEventStreamMatches(
            $eventStore->streamEvents(new StreamUid('Stream1')),
            [],
        );

        $eventStore->write(
            [
                StreamEvent::fromBasicTypes('Stream1', 1, 'Type1', 'Data1'),
                StreamEvent::fromBasicTypes('Stream1', 2, 'Type2', 'Data2'),
                StreamEvent::fromBasicTypes('Stream1', 3, 'Type3', 'Data3'),
            ],
        );

        self::assertNotNull(
            $eventStore->position(new StreamUid('Stream1')),
        );

        self::assertSame(
            3,
            $eventStore->position(new StreamUid('Stream1'))->toInt(),
        );
    }

    /**
     * @dataProvider dataProvider
     */
    public function testWriteAndReadMultipleStreams(TestEventStoreFactoryInterface $factory): void
    {
        $eventStore = $factory->build();

        $eventStore->write(
            [
                StreamEvent::fromBasicTypes('Stream1', 1, 'Type1.1', 'Data1.1'),
                StreamEvent::fromBasicTypes('Stream1', 2, 'Type1.2', 'Data1.2'),
                StreamEvent::fromBasicTypes('Stream2', 1, 'Type2.1', 'Data2.1'),
            ],
        );

        $eventStore->write(
            [
                StreamEvent::fromBasicTypes('Stream2', 2, 'Type2.2', 'Data2.2'),
                StreamEvent::fromBasicTypes('Stream3', 1, 'Type3.1', 'Data3.1'),
                StreamEvent::fromBasicTypes('Stream1', 3, 'Type1.3', 'Data1.3'),
            ],
        );

        $eventStore->write(
            [
                StreamEvent::fromBasicTypes('Stream3', 2, 'Type3.2', 'Data3.2'),
                StreamEvent::fromBasicTypes('Stream3', 3, 'Type3.3', 'Data3.3'),
                StreamEvent::fromBasicTypes('Stream2', 3, 'Type2.3', 'Data2.3'),
            ],
        );

        TestHelper::assertEventStreamMatches(
            $eventStore->allEvents(),
            [
                ['Stream1', 1, 'Type1.1', 'Data1.1'],
                ['Stream1', 2, 'Type1.2', 'Data1.2'],
                ['Stream2', 1, 'Type2.1', 'Data2.1'],
                ['Stream2', 2, 'Type2.2', 'Data2.2'],
                ['Stream3', 1, 'Type3.1', 'Data3.1'],
                ['Stream1', 3, 'Type1.3', 'Data1.3'],
                ['Stream3', 2, 'Type3.2', 'Data3.2'],
                ['Stream3', 3, 'Type3.3', 'Data3.3'],
                ['Stream2', 3, 'Type2.3', 'Data2.3'],
            ],
        );
    }

    /**
     * @dataProvider dataProvider
     */
    public function testFilterFromMultipleStreams(TestEventStoreFactoryInterface $factory): void
    {
        $eventStore = $factory->build();

        $eventStore->write(
            [
                StreamEvent::fromBasicTypes('Stream1', 1, 'Type1', 'Data1.1'),
                StreamEvent::fromBasicTypes('Stream1', 2, 'Type2', 'Data1.2'),
                StreamEvent::fromBasicTypes('Stream2', 1, 'Type1', 'Data2.1'),
            ],
        );

        $eventStore->write(
            [
                StreamEvent::fromBasicTypes('Stream2', 2, 'Type2', 'Data2.2'),
                StreamEvent::fromBasicTypes('Stream3', 1, 'Type1', 'Data3.1'),
                StreamEvent::fromBasicTypes('Stream1', 3, 'Type3', 'Data1.3'),
            ],
        );

        $eventStore->write(
            [
                StreamEvent::fromBasicTypes('Stream3', 2, 'Type2', 'Data3.2'),
                StreamEvent::fromBasicTypes('Stream3', 3, 'Type3', 'Data3.3'),
                StreamEvent::fromBasicTypes('Stream2', 3, 'Type3', 'Data2.3'),
            ],
        );

        $filter = [
            new EventType('Type2'),
            new EventType('Type3'),
        ];

        $beginAfter = TestHelper::assertEventStreamBeginsWith(
            $eventStore->allEvents($filter, null),
            [
                ['Stream1', 2, 'Type2', 'Data1.2'],
                ['Stream2', 2, 'Type2', 'Data2.2'],
                ['Stream1', 3, 'Type3', 'Data1.3'],
            ],
        );

        self::assertNotNull(
            $beginAfter,
        );

        $beginAfter = TestHelper::assertEventStreamBeginsWith(
            $eventStore->allEvents($filter, $beginAfter),
            [
                ['Stream3', 2, 'Type2', 'Data3.2'],
                ['Stream3', 3, 'Type3', 'Data3.3'],
                ['Stream2', 3, 'Type3', 'Data2.3'],
            ],
        );

        self::assertNotNull(
            $beginAfter,
        );

        TestHelper::assertEventStreamMatches(
            $eventStore->allEvents($filter, $beginAfter),
            [],
        );
    }
}
