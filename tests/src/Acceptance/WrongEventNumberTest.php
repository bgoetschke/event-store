<?php

declare(strict_types=1);

namespace BjoernGoetschke\Test\EventStore\Acceptance;

use BjoernGoetschke\EventStore\Event\StreamEvent;
use BjoernGoetschke\EventStore\EventStoreConcurrencyExceptionInterface;
use BjoernGoetschke\EventStore\EventStoreRuntimeExceptionInterface;
use BjoernGoetschke\EventStore\StreamUid;
use BjoernGoetschke\Test\EventStore\TestEventStoreFactoryInterface;
use BjoernGoetschke\Test\EventStore\TestHelper;
use PHPUnit\Framework\TestCase;

final class WrongEventNumberTest extends TestCase
{
    /**
     * @return array<string, array<TestEventStoreFactoryInterface>>
     */
    public function dataProvider(): array
    {
        return TestHelper::eventStoreDataProvider();
    }

    /**
     * @dataProvider dataProvider
     */
    public function testNotStartingWithFirstEventNumberThrowsRuntimeException(
        TestEventStoreFactoryInterface $factory
    ): void {
        $eventStore = $factory->build();

        self::assertNull(
            $eventStore->position(new StreamUid('Stream1')),
        );

        $exception = null;
        try {
            $eventStore->write(
                [
                    StreamEvent::fromBasicTypes('Stream1', 2, 'Type2', 'Data2'),
                ],
            );
        } catch (EventStoreRuntimeExceptionInterface $e) {
            $exception = $e;
        }

        self::assertInstanceOf(
            EventStoreRuntimeExceptionInterface::class,
            $exception,
        );

        self::assertNotInstanceOf(
            EventStoreConcurrencyExceptionInterface::class,
            $exception,
        );

        self::assertNull(
            $eventStore->position(new StreamUid('Stream1')),
        );
    }

    /**
     * @dataProvider dataProvider
     */
    public function testNotStartingWithCorrectEventNumberThrowsRuntimeException(
        TestEventStoreFactoryInterface $factory
    ): void {
        $eventStore = $factory->build();

        self::assertNull(
            $eventStore->position(new StreamUid('Stream1')),
        );

        $eventStore->write(
            [
                StreamEvent::fromBasicTypes('Stream1', 1, 'Type1', 'Data1'),
                StreamEvent::fromBasicTypes('Stream1', 2, 'Type2', 'Data2'),
                StreamEvent::fromBasicTypes('Stream1', 3, 'Type3', 'Data3'),
            ],
        );

        $exception = null;
        try {
            $eventStore->write(
                [
                    StreamEvent::fromBasicTypes('Stream1', 5, 'Type5', 'Data5'),
                ],
            );
        } catch (EventStoreRuntimeExceptionInterface $e) {
            $exception = $e;
        }

        self::assertInstanceOf(
            EventStoreRuntimeExceptionInterface::class,
            $exception,
        );

        self::assertNotInstanceOf(
            EventStoreConcurrencyExceptionInterface::class,
            $exception,
        );

        self::assertNotNull(
            $eventStore->position(new StreamUid('Stream1')),
        );

        self::assertSame(
            3,
            $eventStore->position(new StreamUid('Stream1'))->toInt(),
        );
    }

    /**
     * @dataProvider dataProvider
     */
    public function testStartingWithExistingEventNumberThrowsConcurrencyException(
        TestEventStoreFactoryInterface $factory
    ): void {
        $eventStore = $factory->build();

        self::assertNull(
            $eventStore->position(new StreamUid('Stream1')),
        );

        $eventStore->write(
            [
                StreamEvent::fromBasicTypes('Stream1', 1, 'Type1', 'Data1'),
                StreamEvent::fromBasicTypes('Stream1', 2, 'Type2', 'Data2'),
                StreamEvent::fromBasicTypes('Stream1', 3, 'Type3', 'Data3'),
            ],
        );

        $exception = null;
        try {
            $eventStore->write(
                [
                    StreamEvent::fromBasicTypes('Stream1', 2, 'Type2', 'Data2'),
                ],
            );
        } catch (EventStoreConcurrencyExceptionInterface $e) {
            $exception = $e;
        }

        self::assertInstanceOf(
            EventStoreConcurrencyExceptionInterface::class,
            $exception,
        );

        self::assertMatchesRegularExpression(
            '="Stream1"=',
            $exception->getMessage(),
            'Concurrency exception must contain stream uid.',
        );

        self::assertNotNull(
            $eventStore->position(new StreamUid('Stream1')),
        );

        self::assertSame(
            3,
            $eventStore->position(new StreamUid('Stream1'))->toInt(),
        );
    }

    /**
     * @dataProvider dataProvider
     */
    public function testMissingEventNumberEventNumberThrowsRuntimeException(
        TestEventStoreFactoryInterface $factory
    ): void {
        $eventStore = $factory->build();

        self::assertNull(
            $eventStore->position(new StreamUid('Stream1')),
        );

        $eventStore->write(
            [
                StreamEvent::fromBasicTypes('Stream1', 1, 'Type1', 'Data1'),
                StreamEvent::fromBasicTypes('Stream1', 2, 'Type2', 'Data2'),
                StreamEvent::fromBasicTypes('Stream1', 3, 'Type3', 'Data3'),
            ],
        );

        $exception = null;
        try {
            $eventStore->write(
                [
                    StreamEvent::fromBasicTypes('Stream1', 4, 'Type4', 'Data4'),
                    StreamEvent::fromBasicTypes('Stream1', 5, 'Type5', 'Data5'),
                    StreamEvent::fromBasicTypes('Stream1', 7, 'Type7', 'Data7'),
                ],
            );
        } catch (EventStoreRuntimeExceptionInterface $e) {
            $exception = $e;
        }

        self::assertInstanceOf(
            EventStoreRuntimeExceptionInterface::class,
            $exception,
        );

        self::assertNotInstanceOf(
            EventStoreConcurrencyExceptionInterface::class,
            $exception,
        );

        self::assertNotNull(
            $eventStore->position(new StreamUid('Stream1')),
        );

        self::assertSame(
            3,
            $eventStore->position(new StreamUid('Stream1'))->toInt(),
        );
    }

    /**
     * @dataProvider dataProvider
     */
    public function testSuccessfulWriteAfterConcurrencyException(TestEventStoreFactoryInterface $factory): void
    {
        $eventStore = $factory->build();

        self::assertNull(
            $eventStore->position(new StreamUid('Stream1')),
        );

        $eventStore->write(
            [
                StreamEvent::fromBasicTypes('Stream1', 1, 'Type1', 'Data1'),
                StreamEvent::fromBasicTypes('Stream1', 2, 'Type2', 'Data2'),
                StreamEvent::fromBasicTypes('Stream1', 3, 'Type3', 'Data3'),
            ],
        );

        $exception = null;
        try {
            $eventStore->write(
                [
                    StreamEvent::fromBasicTypes('Stream1', 2, 'Type2', 'Data2'),
                ],
            );
        } catch (EventStoreConcurrencyExceptionInterface $e) {
            $exception = $e;
        }

        self::assertInstanceOf(
            EventStoreConcurrencyExceptionInterface::class,
            $exception,
        );

        $eventStore->write(
            [
                StreamEvent::fromBasicTypes('Stream1', 4, 'Type4', 'Data4'),
                StreamEvent::fromBasicTypes('Stream1', 5, 'Type5', 'Data5'),
                StreamEvent::fromBasicTypes('Stream1', 6, 'Type6', 'Data6'),
            ],
        );

        self::assertNotNull(
            $eventStore->position(new StreamUid('Stream1')),
        );

        self::assertSame(
            6,
            $eventStore->position(new StreamUid('Stream1'))->toInt(),
        );
    }
}
