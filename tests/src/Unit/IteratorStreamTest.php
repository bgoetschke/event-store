<?php

declare(strict_types=1);

namespace BjoernGoetschke\Test\EventStore\Unit;

use BjoernGoetschke\EventStore\Stream\EventStreamEntry;
use BjoernGoetschke\EventStore\Stream\IteratorEventStream;
use BjoernGoetschke\EventStore\Stream\IteratorUidStream;
use BjoernGoetschke\EventStore\StreamUid;
use PHPUnit\Framework\TestCase;

final class IteratorStreamTest extends TestCase
{
    public function testEventStream(): void
    {
        $events = [
            EventStreamEntry::fromBasicTypes(
                'Reference1',
                'Stream1',
                1,
                'Type1',
                'Data1',
            ),
            EventStreamEntry::fromBasicTypes(
                'Reference2',
                'Stream1',
                2,
                'Type2',
                'Data2',
            ),
            EventStreamEntry::fromBasicTypes(
                'Reference3',
                'Stream1',
                3,
                'Type3',
                'Data3',
            ),
        ];

        $generator = function () use ($events) {
            foreach ($events as $event) {
                yield $event;
            }
        };
        $stream = new IteratorEventStream($generator());

        self::assertNull(
            $stream->reference(),
        );

        $entry1 = array_shift($events);
        self::assertSame(
            $entry1->event(),
            $stream->next(),
        );
        self::assertNotNull(
            $stream->reference(),
        );
        self::assertSame(
            $entry1->reference(),
            $stream->reference(),
        );

        $entry2 = array_shift($events);
        self::assertSame(
            $entry2->event(),
            $stream->next(),
        );
        self::assertNotNull(
            $stream->reference(),
        );
        self::assertSame(
            $entry2->reference(),
            $stream->reference(),
        );

        $entry3 = array_shift($events);
        self::assertSame(
            $entry3->event(),
            $stream->next(),
        );
        self::assertNotNull(
            $stream->reference(),
        );
        self::assertSame(
            $entry3->reference(),
            $stream->reference(),
        );

        self::assertNull(
            $stream->next(),
        );

        self::assertCount(
            0,
            $events,
        );
    }

    public function testUidStream(): void
    {
        $uids = [
            new StreamUid('Stream1'),
            new StreamUid('Stream2'),
            new StreamUid('Stream3'),
        ];

        $generator = function () use ($uids) {
            foreach ($uids as $uid) {
                yield $uid;
            }
        };
        $stream = new IteratorUidStream($generator());

        self::assertSame(
            array_shift($uids),
            $stream->next(),
        );

        self::assertSame(
            array_shift($uids),
            $stream->next(),
        );

        self::assertSame(
            array_shift($uids),
            $stream->next(),
        );

        self::assertNull(
            $stream->next(),
        );

        self::assertCount(
            0,
            $uids,
        );
    }
}
