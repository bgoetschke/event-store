<?php

declare(strict_types=1);

namespace BjoernGoetschke\Test\EventStore\Unit;

use BjoernGoetschke\EventStore\Event\EventData;
use BjoernGoetschke\EventStore\Event\EventNumber;
use BjoernGoetschke\EventStore\Event\EventType;
use BjoernGoetschke\EventStore\Event\StreamEvent;
use BjoernGoetschke\EventStore\EventReference;
use BjoernGoetschke\EventStore\StreamUid;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

final class BasicTypesTest extends TestCase
{
    public function testConvertStreamUid(): void
    {
        $uid = new StreamUid('SomeUid');

        self::assertSame(
            'SomeUid',
            $uid->toString(),
        );

        self::assertSame(
            'SomeUid',
            (string)$uid,
        );
    }

    public function testEmptyStreamUidThrowsException(): void
    {
        $this->expectException(InvalidArgumentException::class);

        new StreamUid('');
    }

    public function testTooLongStreamUidThrowsException(): void
    {
        $uid = new StreamUid(str_pad('', 128, 'X'));

        self::assertSame(
            str_pad('', 128, 'X'),
            (string)$uid,
        );

        $this->expectException(InvalidArgumentException::class);

        new StreamUid(str_pad('', 129, 'X'));
    }

    public function testConvertEventNumber(): void
    {
        $number = new EventNumber(1);

        self::assertSame(
            1,
            $number->toInt(),
        );

        self::assertSame(
            '1',
            $number->toString(),
        );

        self::assertSame(
            '1',
            (string)$number,
        );
    }

    public function testConvertEventNumberMaxInt(): void
    {
        $number = new EventNumber(PHP_INT_MAX);

        self::assertSame(
            PHP_INT_MAX,
            $number->toInt(),
        );

        self::assertSame(
            (string)PHP_INT_MAX,
            $number->toString(),
        );

        self::assertSame(
            (string)PHP_INT_MAX,
            (string)$number,
        );
    }

    public function testZeroOrBelowEventNumberThrowsException(): void
    {
        $this->expectException(InvalidArgumentException::class);

        new EventNumber(0);
    }

    public function testConvertEventType(): void
    {
        $type = new EventType('SomeType');

        self::assertSame(
            'SomeType',
            $type->toString(),
        );

        self::assertSame(
            'SomeType',
            (string)$type,
        );
    }

    public function testEmptyEventTypeThrowsException(): void
    {
        $this->expectException(InvalidArgumentException::class);

        new EventType('');
    }

    public function testTooLongEventTypeThrowsException(): void
    {
        $type = new EventType(str_pad('', 128, 'X'));

        self::assertSame(
            str_pad('', 128, 'X'),
            (string)$type,
        );

        $this->expectException(InvalidArgumentException::class);

        new EventType(str_pad('', 129, 'X'));
    }

    public function testConvertEventData(): void
    {
        $data = new EventData('SomeData');

        self::assertSame(
            'SomeData',
            $data->toString(),
        );

        self::assertSame(
            'SomeData',
            (string)$data,
        );
    }

    public function testConvertData1KiB(): void
    {
        $bytes = 1024;

        $data = new EventData(str_pad('', $bytes, 'X'));

        self::assertSame(
            str_pad('', $bytes, 'X'),
            (string)$data,
        );
    }

    public function testConvertData1MiB(): void
    {
        $bytes = 1024 * 1024;

        $data = new EventData(str_pad('', $bytes, 'X'));

        self::assertSame(
            str_pad('', $bytes, 'X'),
            (string)$data,
        );
    }

    public function testConvertData16MiB(): void
    {
        $bytes = 1024 * 1024 * 16;

        $data = new EventData(str_pad('', $bytes, 'X'));

        self::assertSame(
            str_pad('', $bytes, 'X'),
            (string)$data,
        );
    }

    public function testStreamEventReturnsCorrectValues(): void
    {
        $stream = new StreamUid('SomeStream');
        $number = new EventNumber(1);
        $type = new EventType('SomeType');
        $data = new EventData('SomeData');
        $event = new StreamEvent($stream, $number, $type, $data);

        self::assertSame(
            $stream,
            $event->streamUid(),
        );

        self::assertSame(
            $number,
            $event->eventNumber(),
        );

        self::assertSame(
            $type,
            $event->eventType(),
        );

        self::assertSame(
            $data,
            $event->eventData(),
        );
    }

    public function testConvertEventReference(): void
    {
        $reference = new EventReference('SomeReference');

        self::assertSame(
            'SomeReference',
            $reference->toString(),
        );

        self::assertSame(
            'SomeReference',
            (string)$reference,
        );
    }

    public function testEmptyEventReferenceThrowsException(): void
    {
        $this->expectException(InvalidArgumentException::class);

        new EventReference('');
    }

    public function testTooLongEventReferenceThrowsException(): void
    {
        $reference = new EventReference(str_pad('', 128, 'X'));

        self::assertSame(
            str_pad('', 128, 'X'),
            (string)$reference,
        );

        $this->expectException(InvalidArgumentException::class);

        new EventReference(str_pad('', 129, 'X'));
    }

    public function testCreateStreamEventFromBasicTypes(): void
    {
        $event = StreamEvent::fromBasicTypes(
            'SomeStream',
            1,
            'SomeType',
            'SomeData',
        );

        self::assertSame(
            'SomeStream',
            $event->streamUid()->toString(),
        );

        self::assertSame(
            1,
            $event->eventNumber()->toInt(),
        );

        self::assertSame(
            'SomeType',
            $event->eventType()->toString(),
        );

        self::assertSame(
            'SomeData',
            $event->eventData()->toString(),
        );
    }
}
