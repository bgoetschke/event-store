<?php

declare(strict_types=1);

namespace BjoernGoetschke\Test\EventStore\Integration;

use BjoernGoetschke\EventStore\Event\StreamEvent;
use BjoernGoetschke\EventStore\EventStoreConcurrencyExceptionInterface;
use BjoernGoetschke\EventStore\EventStoreRuntimeExceptionInterface;
use BjoernGoetschke\EventStore\StreamUid;
use BjoernGoetschke\Test\EventStore\TestEventStoreFactoryInterface;
use BjoernGoetschke\Test\EventStore\TestHelper;
use BjoernGoetschke\Test\EventStore\TestPdoEventStoreFactory;
use PDO;
use PHPUnit\Framework\TestCase;

final class PdoTransactionHandlingTest extends TestCase
{
    /**
     * @return array<string, array<TestEventStoreFactoryInterface>>
     */
    public function dataProvider(): array
    {
        return TestHelper::eventStoreDataProvider(TestPdoEventStoreFactory::class);
    }

    /**
     * @dataProvider dataProvider
     */
    public function testOuterTransactionIsStillOpenAfterSuccessfulWrite(TestPdoEventStoreFactory $factory): void
    {
        $eventStore = $factory->buildPdoEventStore();
        TestHelper::modifyPdoEventStore($eventStore, PDO::ERRMODE_SILENT, false);
        TestHelper::beginPdoEventStoreTransaction($eventStore);

        TestHelper::assertPdoEventStoreTransaction(
            $eventStore,
            true,
        );

        $eventStore->write([]);

        TestHelper::assertPdoEventStoreTransaction(
            $eventStore,
            true,
        );
    }

    /**
     * @dataProvider dataProvider
     */
    public function testInnerTransactionIsCommittedAfterSuccessfulWrite(TestPdoEventStoreFactory $factory): void
    {
        $eventStore = $factory->buildPdoEventStore();
        TestHelper::modifyPdoEventStore($eventStore, PDO::ERRMODE_SILENT, false);

        TestHelper::assertPdoEventStoreTransaction(
            $eventStore,
            false,
        );

        $eventStore->write([]);

        TestHelper::assertPdoEventStoreTransaction(
            $eventStore,
            false,
        );
    }

    /**
     * @dataProvider dataProvider
     */
    public function testOuterTransactionIsStillOpenAfterThrownPdoException(TestPdoEventStoreFactory $factory): void
    {
        $eventStore = $factory->buildPdoEventStore();
        TestHelper::modifyPdoEventStore($eventStore, PDO::ERRMODE_SILENT, true);
        TestHelper::beginPdoEventStoreTransaction($eventStore);

        TestHelper::assertPdoEventStoreTransaction(
            $eventStore,
            true,
        );

        $exception = null;
        try {
            $eventStore->write(
                [
                    StreamEvent::fromBasicTypes('Stream1', 1, 'Type1', 'Data1'),
                ],
            );
        } catch (EventStoreRuntimeExceptionInterface $e) {
            $exception = $e;
        }
        self::assertInstanceOf(
            EventStoreRuntimeExceptionInterface::class,
            $exception,
        );
        self::assertNotInstanceOf(
            EventStoreConcurrencyExceptionInterface::class,
            $exception,
        );

        TestHelper::assertPdoEventStoreTransaction(
            $eventStore,
            true,
        );
    }

    /**
     * @dataProvider dataProvider
     */
    public function testInnerTransactionIsRolledBackAfterThrownPdoException(TestPdoEventStoreFactory $factory): void
    {
        $eventStore = $factory->buildPdoEventStore();
        TestHelper::modifyPdoEventStore($eventStore, PDO::ERRMODE_SILENT, true);

        TestHelper::assertPdoEventStoreTransaction(
            $eventStore,
            false,
        );

        $exception = null;
        try {
            $eventStore->write(
                [
                    StreamEvent::fromBasicTypes('Stream1', 1, 'Type1', 'Data1'),
                ],
            );
        } catch (EventStoreRuntimeExceptionInterface $e) {
            $exception = $e;
        }
        self::assertInstanceOf(
            EventStoreRuntimeExceptionInterface::class,
            $exception,
        );
        self::assertNotInstanceOf(
            EventStoreConcurrencyExceptionInterface::class,
            $exception,
        );

        TestHelper::assertPdoEventStoreTransaction(
            $eventStore,
            false,
        );
    }

    /**
     * @dataProvider dataProvider
     */
    public function testOuterTransactionIsStillOpenAfterThrownConcurrencyException(
        TestPdoEventStoreFactory $factory
    ): void {
        $eventStore = $factory->buildPdoEventStore();
        TestHelper::modifyPdoEventStore($eventStore, PDO::ERRMODE_SILENT, false);
        TestHelper::beginPdoEventStoreTransaction($eventStore);

        TestHelper::assertPdoEventStoreTransaction(
            $eventStore,
            true,
        );

        $events = [
            StreamEvent::fromBasicTypes('Stream1', 1, 'Type1', 'Data1'),
            StreamEvent::fromBasicTypes('Stream1', 2, 'Type2', 'Data2'),
            StreamEvent::fromBasicTypes('Stream1', 3, 'Type3', 'Data3'),
        ];

        $eventStore->write($events);

        $exception = null;
        try {
            $eventStore->write($events);
        } catch (EventStoreConcurrencyExceptionInterface $e) {
            $exception = $e;
        }
        self::assertInstanceOf(
            EventStoreConcurrencyExceptionInterface::class,
            $exception,
        );

        TestHelper::assertPdoEventStoreTransaction(
            $eventStore,
            true,
        );
    }

    /**
     * @dataProvider dataProvider
     */
    public function testInnerTransactionIsRolledBackAfterThrownConcurrencyException(
        TestPdoEventStoreFactory $factory
    ): void {
        $eventStore = $factory->buildPdoEventStore();
        TestHelper::modifyPdoEventStore($eventStore, PDO::ERRMODE_SILENT, false);

        TestHelper::assertPdoEventStoreTransaction(
            $eventStore,
            false,
        );

        $events = [
            StreamEvent::fromBasicTypes('Stream1', 1, 'Type1', 'Data1'),
            StreamEvent::fromBasicTypes('Stream1', 2, 'Type2', 'Data2'),
            StreamEvent::fromBasicTypes('Stream1', 3, 'Type3', 'Data3'),
        ];

        $eventStore->write($events);

        $exception = null;
        try {
            $eventStore->write($events);
        } catch (EventStoreConcurrencyExceptionInterface $e) {
            $exception = $e;
        }
        self::assertInstanceOf(
            EventStoreConcurrencyExceptionInterface::class,
            $exception,
        );

        TestHelper::assertPdoEventStoreTransaction(
            $eventStore,
            false,
        );
    }

    /**
     * @dataProvider dataProvider
     */
    public function testNoEventsWrittenOnConcurrencyExceptionWithOuterTransaction(
        TestPdoEventStoreFactory $factory
    ): void {
        $eventStore = $factory->buildPdoEventStore();
        TestHelper::modifyPdoEventStore($eventStore, PDO::ERRMODE_SILENT, false);
        TestHelper::beginPdoEventStoreTransaction($eventStore);

        TestHelper::assertPdoEventStoreTransaction(
            $eventStore,
            true,
        );

        $events = [
            StreamEvent::fromBasicTypes('Stream1', 1, 'Type1', 'Data1'),
        ];

        $eventStore->write($events);

        $exception = null;
        try {
            $eventStore->write($events);
        } catch (EventStoreConcurrencyExceptionInterface $e) {
            $exception = $e;
        }
        self::assertInstanceOf(
            EventStoreConcurrencyExceptionInterface::class,
            $exception,
        );

        self::assertNotNull(
            $eventStore->position(new StreamUid('Stream1')),
        );

        self::assertSame(
            1,
            $eventStore->position(new StreamUid('Stream1'))->toInt(),
        );

        TestHelper::assertPdoEventStoreTransaction(
            $eventStore,
            true,
        );
    }

    /**
     * @dataProvider dataProvider
     */
    public function testNoEventsWrittenOnConcurrencyExceptionWithInnerTransaction(
        TestPdoEventStoreFactory $factory
    ): void {
        $eventStore = $factory->buildPdoEventStore();
        TestHelper::modifyPdoEventStore($eventStore, PDO::ERRMODE_SILENT, false);

        $events = [
            StreamEvent::fromBasicTypes('Stream1', 1, 'Type1', 'Data1'),
        ];

        $eventStore->write($events);

        $exception = null;
        try {
            $eventStore->write($events);
        } catch (EventStoreConcurrencyExceptionInterface $e) {
            $exception = $e;
        }
        self::assertInstanceOf(
            EventStoreConcurrencyExceptionInterface::class,
            $exception,
        );

        self::assertNotNull(
            $eventStore->position(new StreamUid('Stream1')),
        );

        self::assertSame(
            1,
            $eventStore->position(new StreamUid('Stream1'))->toInt(),
        );
    }

    /**
     * @dataProvider dataProvider
     */
    public function testNoEventsWrittenOnRuntimeExceptionWithOuterTransaction(
        TestPdoEventStoreFactory $factory
    ): void {
        $eventStore = $factory->buildPdoEventStore();
        TestHelper::modifyPdoEventStore($eventStore, PDO::ERRMODE_SILENT, false);
        TestHelper::beginPdoEventStoreTransaction($eventStore);

        $eventStore->write(
            [
                StreamEvent::fromBasicTypes('Stream1', 1, 'Type1', 'Data1'),
                StreamEvent::fromBasicTypes('Stream1', 2, 'Type2', 'Data2'),
            ],
        );

        $exception = null;
        try {
            $eventStore->write(
                [
                    StreamEvent::fromBasicTypes('Stream1', 3, 'Type3', 'Data3'),
                    StreamEvent::fromBasicTypes('Stream1', 4, 'Type4', 'Data4'),
                    StreamEvent::fromBasicTypes('Stream1', 6, 'Type6', 'Data6'),
                ],
            );
        } catch (EventStoreRuntimeExceptionInterface $e) {
            $exception = $e;
        }
        self::assertInstanceOf(
            EventStoreRuntimeExceptionInterface::class,
            $exception,
        );
        self::assertNotInstanceOf(
            EventStoreConcurrencyExceptionInterface::class,
            $exception,
        );

        self::assertNotNull(
            $eventStore->position(new StreamUid('Stream1')),
        );

        self::assertSame(
            2,
            $eventStore->position(new StreamUid('Stream1'))->toInt(),
        );
    }

    /**
     * @dataProvider dataProvider
     */
    public function testNoEventsWrittenOnRuntimeExceptionWithInnerTransaction(
        TestPdoEventStoreFactory $factory
    ): void {
        $eventStore = $factory->buildPdoEventStore();
        TestHelper::modifyPdoEventStore($eventStore, PDO::ERRMODE_SILENT, false);

        $eventStore->write(
            [
                StreamEvent::fromBasicTypes('Stream1', 1, 'Type1', 'Data1'),
                StreamEvent::fromBasicTypes('Stream1', 2, 'Type2', 'Data2'),
            ],
        );

        $exception = null;
        try {
            $eventStore->write(
                [
                    StreamEvent::fromBasicTypes('Stream1', 3, 'Type3', 'Data3'),
                    StreamEvent::fromBasicTypes('Stream1', 4, 'Type4', 'Data4'),
                    StreamEvent::fromBasicTypes('Stream1', 6, 'Type6', 'Data6'),
                ],
            );
        } catch (EventStoreRuntimeExceptionInterface $e) {
            $exception = $e;
        }
        self::assertInstanceOf(
            EventStoreRuntimeExceptionInterface::class,
            $exception,
        );
        self::assertNotInstanceOf(
            EventStoreConcurrencyExceptionInterface::class,
            $exception,
        );

        self::assertNotNull(
            $eventStore->position(new StreamUid('Stream1')),
        );

        self::assertSame(
            2,
            $eventStore->position(new StreamUid('Stream1'))->toInt(),
        );
    }
}
