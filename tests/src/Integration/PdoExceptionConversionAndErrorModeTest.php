<?php

declare(strict_types=1);

namespace BjoernGoetschke\Test\EventStore\Integration;

use BjoernGoetschke\EventStore\Event\StreamEvent;
use BjoernGoetschke\EventStore\EventReference;
use BjoernGoetschke\EventStore\EventStoreRuntimeExceptionInterface;
use BjoernGoetschke\EventStore\StreamUid;
use BjoernGoetschke\Test\EventStore\TestEventStoreFactoryInterface;
use BjoernGoetschke\Test\EventStore\TestHelper;
use BjoernGoetschke\Test\EventStore\TestPdoEventStoreFactory;
use PDO;
use PHPUnit\Framework\TestCase;

final class PdoExceptionConversionAndErrorModeTest extends TestCase
{
    /**
     * @return array<string, array<TestEventStoreFactoryInterface>>
     */
    public function dataProvider(): array
    {
        return TestHelper::eventStoreDataProvider(TestPdoEventStoreFactory::class);
    }

    /**
     * @dataProvider dataProvider
     */
    public function testStreamsMethodThrowsCorrectExceptionAndKeepsPdoErrorMode(
        TestPdoEventStoreFactory $factory
    ): void {
        $eventStore = $factory->buildPdoEventStore();
        TestHelper::modifyPdoEventStore($eventStore, PDO::ERRMODE_SILENT, true);

        $exception = null;
        try {
            $eventStore->streams();
        } catch (EventStoreRuntimeExceptionInterface $e) {
            $exception = $e;
        }
        self::assertInstanceOf(
            EventStoreRuntimeExceptionInterface::class,
            $exception,
        );

        TestHelper::assertPdoEventStoreErrorMode(
            $eventStore,
            PDO::ERRMODE_SILENT,
        );
    }

    /**
     * @dataProvider dataProvider
     */
    public function testPositionMethodThrowsCorrectExceptionAndKeepsPdoErrorMode(
        TestPdoEventStoreFactory $factory
    ): void {
        $eventStore = $factory->buildPdoEventStore();
        TestHelper::modifyPdoEventStore($eventStore, PDO::ERRMODE_SILENT, true);

        $exception = null;
        try {
            $eventStore->position(new StreamUid('SomeNotExistingUid'));
        } catch (EventStoreRuntimeExceptionInterface $e) {
            $exception = $e;
        }
        self::assertInstanceOf(
            EventStoreRuntimeExceptionInterface::class,
            $exception,
        );

        TestHelper::assertPdoEventStoreErrorMode(
            $eventStore,
            PDO::ERRMODE_SILENT,
        );
    }

    /**
     * @dataProvider dataProvider
     */
    public function testStreamEventsMethodThrowsCorrectExceptionAndKeepsPdoErrorMode(
        TestPdoEventStoreFactory $factory
    ): void {
        $eventStore = $factory->buildPdoEventStore();
        TestHelper::modifyPdoEventStore($eventStore, PDO::ERRMODE_SILENT, true);

        $exception = null;
        try {
            $eventStore->streamEvents(new StreamUid('SomeNotExistingUid'));
        } catch (EventStoreRuntimeExceptionInterface $e) {
            $exception = $e;
        }
        self::assertInstanceOf(
            EventStoreRuntimeExceptionInterface::class,
            $exception,
        );

        TestHelper::assertPdoEventStoreErrorMode(
            $eventStore,
            PDO::ERRMODE_SILENT,
        );
    }

    /**
     * @dataProvider dataProvider
     */
    public function testAllEventsMethodThrowsCorrectExceptionAndKeepsPdoErrorMode(
        TestPdoEventStoreFactory $factory
    ): void {
        $eventStore = $factory->buildPdoEventStore();
        TestHelper::modifyPdoEventStore($eventStore, PDO::ERRMODE_SILENT, true);

        $exception = null;
        try {
            $eventStore->allEvents();
        } catch (EventStoreRuntimeExceptionInterface $e) {
            $exception = $e;
        }
        self::assertInstanceOf(
            EventStoreRuntimeExceptionInterface::class,
            $exception,
        );

        TestHelper::assertPdoEventStoreErrorMode(
            $eventStore,
            PDO::ERRMODE_SILENT,
        );
    }

    /**
     * @dataProvider dataProvider
     */
    public function testAllEventsMethodThrowsCorrectExceptionAndKeepsPdoErrorModeOnInvalidReference(
        TestPdoEventStoreFactory $factory
    ): void {
        $eventStore = $factory->buildPdoEventStore();
        TestHelper::modifyPdoEventStore($eventStore, PDO::ERRMODE_SILENT, false);

        $exception = null;
        try {
            $eventStore->allEvents([], new EventReference('InvalidReference'));
        } catch (EventStoreRuntimeExceptionInterface $e) {
            $exception = $e;
        }
        self::assertInstanceOf(
            EventStoreRuntimeExceptionInterface::class,
            $exception,
        );

        TestHelper::assertPdoEventStoreErrorMode(
            $eventStore,
            PDO::ERRMODE_SILENT,
        );
    }

    /**
     * @dataProvider dataProvider
     */
    public function testAllEventsMethodThrowsCorrectExceptionAndKeepsPdoErrorModeOnNotExistingReference(
        TestPdoEventStoreFactory $factory
    ): void {
        $eventStore = $factory->buildPdoEventStore();
        TestHelper::modifyPdoEventStore($eventStore, PDO::ERRMODE_SILENT, false);

        $exception = null;
        try {
            $eventStore->allEvents([], new EventReference('1'));
        } catch (EventStoreRuntimeExceptionInterface $e) {
            $exception = $e;
        }
        self::assertInstanceOf(
            EventStoreRuntimeExceptionInterface::class,
            $exception,
        );

        TestHelper::assertPdoEventStoreErrorMode(
            $eventStore,
            PDO::ERRMODE_SILENT,
        );
    }

    /**
     * @dataProvider dataProvider
     */
    public function testWriteMethodThrowsCorrectExceptionAndKeepsPdoErrorMode(
        TestPdoEventStoreFactory $factory
    ): void {
        $eventStore = $factory->buildPdoEventStore();
        TestHelper::modifyPdoEventStore($eventStore, PDO::ERRMODE_SILENT, true);

        $exception = null;
        try {
            $eventStore->write(
                [
                    StreamEvent::fromBasicTypes('Stream1', 1, 'Type1', 'Data1'),
                ],
            );
        } catch (EventStoreRuntimeExceptionInterface $e) {
            $exception = $e;
        }
        self::assertInstanceOf(
            EventStoreRuntimeExceptionInterface::class,
            $exception,
        );

        TestHelper::assertPdoEventStoreErrorMode(
            $eventStore,
            PDO::ERRMODE_SILENT,
        );
    }
}
