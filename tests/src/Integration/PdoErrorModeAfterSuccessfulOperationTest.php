<?php

declare(strict_types=1);

namespace BjoernGoetschke\Test\EventStore\Integration;

use BjoernGoetschke\EventStore\StreamUid;
use BjoernGoetschke\Test\EventStore\TestHelper;
use BjoernGoetschke\Test\EventStore\TestPdoEventStoreFactory;
use PDO;
use PHPUnit\Framework\TestCase;

final class PdoErrorModeAfterSuccessfulOperationTest extends TestCase
{
    /**
     * @return array<string, array<TestPdoEventStoreFactory>>
     */
    public function dataProvider(): array
    {
        return TestHelper::eventStoreDataProvider(TestPdoEventStoreFactory::class);
    }

    /**
     * @dataProvider dataProvider
     */
    public function testStreamsMethodKeepsPdoErrorMode(TestPdoEventStoreFactory $factory): void
    {
        $eventStore = $factory->buildPdoEventStore();
        TestHelper::modifyPdoEventStore($eventStore, PDO::ERRMODE_SILENT, false);

        $eventStore->streams();

        TestHelper::assertPdoEventStoreErrorMode(
            $eventStore,
            PDO::ERRMODE_SILENT,
        );
    }

    /**
     * @dataProvider dataProvider
     */
    public function testStreamsMethodDoesNotAlterCorrectErrorMode(TestPdoEventStoreFactory $factory): void
    {
        $eventStore = $factory->buildPdoEventStore();
        TestHelper::modifyPdoEventStore($eventStore, PDO::ERRMODE_EXCEPTION, false);

        $eventStore->streams();

        TestHelper::assertPdoEventStoreErrorMode(
            $eventStore,
            PDO::ERRMODE_EXCEPTION,
        );
    }

    /**
     * @dataProvider dataProvider
     */
    public function testPositionMethodKeepsPdoErrorMode(TestPdoEventStoreFactory $factory): void
    {
        $eventStore = $factory->buildPdoEventStore();
        TestHelper::modifyPdoEventStore($eventStore, PDO::ERRMODE_SILENT, false);

        $eventStore->position(new StreamUid('SomeNotExistingUid'));

        TestHelper::assertPdoEventStoreErrorMode(
            $eventStore,
            PDO::ERRMODE_SILENT,
        );
    }

    /**
     * @dataProvider dataProvider
     */
    public function testPositionMethodDoesNotAlterCorrectErrorMode(TestPdoEventStoreFactory $factory): void
    {
        $eventStore = $factory->buildPdoEventStore();
        TestHelper::modifyPdoEventStore($eventStore, PDO::ERRMODE_EXCEPTION, false);

        $eventStore->position(new StreamUid('SomeNotExistingUid'));

        TestHelper::assertPdoEventStoreErrorMode(
            $eventStore,
            PDO::ERRMODE_EXCEPTION,
        );
    }

    /**
     * @dataProvider dataProvider
     */
    public function testEventsMethodKeepsPdoErrorMode(TestPdoEventStoreFactory $factory): void
    {
        $eventStore = $factory->buildPdoEventStore();
        TestHelper::modifyPdoEventStore($eventStore, PDO::ERRMODE_SILENT, false);

        $eventStore->streamEvents(new StreamUid('SomeNotExistingUid'));

        TestHelper::assertPdoEventStoreErrorMode(
            $eventStore,
            PDO::ERRMODE_SILENT,
        );
    }

    /**
     * @dataProvider dataProvider
     */
    public function testEventsMethodDoesNotAlterCorrectErrorMode(TestPdoEventStoreFactory $factory): void
    {
        $eventStore = $factory->buildPdoEventStore();
        TestHelper::modifyPdoEventStore($eventStore, PDO::ERRMODE_EXCEPTION, false);

        $eventStore->streamEvents(new StreamUid('SomeNotExistingUid'));

        TestHelper::assertPdoEventStoreErrorMode(
            $eventStore,
            PDO::ERRMODE_EXCEPTION,
        );
    }

    /**
     * @dataProvider dataProvider
     */
    public function testWriteMethodKeepsPdoErrorMode(TestPdoEventStoreFactory $factory): void
    {
        $eventStore = $factory->buildPdoEventStore();
        TestHelper::modifyPdoEventStore($eventStore, PDO::ERRMODE_SILENT, false);

        $eventStore->write([]);

        TestHelper::assertPdoEventStoreErrorMode(
            $eventStore,
            PDO::ERRMODE_SILENT,
        );
    }

    /**
     * @dataProvider dataProvider
     */
    public function testWriteMethodDoesNotAlterCorrectErrorMode(TestPdoEventStoreFactory $factory): void
    {
        $eventStore = $factory->buildPdoEventStore();
        TestHelper::modifyPdoEventStore($eventStore, PDO::ERRMODE_EXCEPTION, false);

        $eventStore->write([]);

        TestHelper::assertPdoEventStoreErrorMode(
            $eventStore,
            PDO::ERRMODE_EXCEPTION,
        );
    }
}
