<?php

declare(strict_types=1);

namespace BjoernGoetschke\Test\EventStore;

use BjoernGoetschke\EventStore\EventStoreInterface;
use BjoernGoetschke\EventStore\PdoEventStore;
use Closure;

final class TestPdoEventStoreFactory implements TestEventStoreFactoryInterface
{
    private TestEventStoreFactory $parent;

    /**
     * @param Closure(): PdoEventStore $factory
     */
    public function __construct(Closure $factory)
    {
        $this->parent = new TestEventStoreFactory($factory);
    }

    public function build(): EventStoreInterface
    {
        return $this->parent->build();
    }

    public function buildPdoEventStore(): PdoEventStore
    {
        $eventStore = $this->build();
        assert($eventStore instanceof PdoEventStore);
        return $eventStore;
    }
}
