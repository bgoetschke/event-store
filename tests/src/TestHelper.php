<?php

declare(strict_types=1);

namespace BjoernGoetschke\Test\EventStore;

use BjoernGoetschke\EventStore\EventReference;
use BjoernGoetschke\EventStore\PdoEventStore;
use BjoernGoetschke\EventStore\Stream\EventStreamInterface;
use BjoernGoetschke\EventStore\Stream\UidStreamInterface;
use PDO;
use PHPUnit\Framework\TestCase;
use ReflectionClass;

final class TestHelper
{
    /**
     * @var TestEventStoreFactoryInterface[]
     */
    private static array $eventStoreDataProvider = [];

    public static function registerEventStoreDataProvider(string $name, TestEventStoreFactoryInterface $factory): void
    {
        self::$eventStoreDataProvider[$name] = $factory;
    }

    public static function registerSqliteEventStoreDataProvider(
        string $name,
        ?string $dsn,
        ?string $username,
        ?string $password
    ): void {
        self::registerEventStoreDataProvider(
            $name,
            new TestPdoEventStoreFactory(
                function () use ($dsn, $username, $password): PdoEventStore {
                    assert(is_string($dsn));
                    $pdo = new PDO(
                        $dsn,
                        $username,
                        $password,
                        [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION],
                    );
                    $pdo->exec(/** @lang SQLite */ "PRAGMA foreign_keys = OFF");
                    $selectTablesSql = /** @lang SQLite */
                        "SELECT `name` FROM `sqlite_master` " .
                        "WHERE `type` = 'table' AND `name` NOT LIKE 'sqlite_%'";
                    $selectTablesStatement = $pdo->query($selectTablesSql);
                    assert($selectTablesStatement !== false);
                    $tables = $selectTablesStatement->fetchAll(PDO::FETCH_NUM);
                    assert($tables !== false);
                    foreach ($tables as $row) {
                        $pdo->exec(/** @lang SQLite */ "DROP TABLE `" . $row[0] . "`");
                    }
                    $pdo->exec(/** @lang SQLite */ "PRAGMA foreign_keys = ON");
                    $createTablesSql = file_get_contents(__DIR__ . '/../../contrib/create_tables_sqlite.sql');
                    assert($createTablesSql !== false);
                    $pdo->exec($createTablesSql);
                    return new PdoEventStore($pdo, 'UID_TABLE', 'EVENT_TABLE');
                },
            ),
        );
    }

    public static function registerMySQLEventStoreDataProvider(
        string $name,
        ?string $dsn,
        ?string $username,
        ?string $password
    ): void {
        self::registerEventStoreDataProvider(
            $name,
            new TestPdoEventStoreFactory(
                function () use ($dsn, $username, $password): PdoEventStore {
                    assert(is_string($dsn));
                    $pdo = new PDO(
                        $dsn,
                        $username,
                        $password,
                        [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION],
                    );
                    $pdo->exec(/** @lang MySQL */ "SET FOREIGN_KEY_CHECKS = 0");
                    $selectTablesSql = /** @lang MySQL */
                        "SHOW TABLES LIKE '%'";
                    $selectTablesStatement = $pdo->query($selectTablesSql);
                    assert($selectTablesStatement !== false);
                    $tables = $selectTablesStatement->fetchAll(PDO::FETCH_NUM);
                    assert($tables !== false);
                    foreach ($tables as $row) {
                        $pdo->exec(/** @lang MySQL */ "DROP TABLE `" . $row[0] . "`");
                    }
                    $pdo->exec(/** @lang MySQL */ "SET FOREIGN_KEY_CHECKS = 1");
                    $createTablesSql = file_get_contents(__DIR__ . '/../../contrib/create_tables_mysql.sql');
                    assert($createTablesSql !== false);
                    $pdo->exec($createTablesSql);
                    return new PdoEventStore($pdo, 'UID_TABLE', 'EVENT_TABLE');
                },
            ),
        );
    }

    /**
     * @template T of TestEventStoreFactoryInterface
     * @param class-string<T> $typeFilter
     * @return array<string, array<T>>
     */
    public static function eventStoreDataProvider(string $typeFilter = TestEventStoreFactoryInterface::class): array
    {
        return array_map(
            function (TestEventStoreFactoryInterface $factory): array {
                return [$factory];
            },
            array_filter(
                self::$eventStoreDataProvider,
                function (TestEventStoreFactoryInterface $factory) use ($typeFilter): bool {
                    return $factory instanceof $typeFilter;
                },
            ),
        );
    }

    public static function modifyPdoEventStore(PdoEventStore $eventStore, int $pdoErrMode, bool $invalidTables): void
    {
        $reflection = new ReflectionClass($eventStore);
        $pdoProperty = $reflection->getProperty('pdo');
        $pdoProperty->setAccessible(true);
        $uidTableProperty = $reflection->getProperty('uidTable');
        $uidTableProperty->setAccessible(true);
        $eventTableProperty = $reflection->getProperty('eventTable');
        $eventTableProperty->setAccessible(true);

        $pdo = $pdoProperty->getValue($eventStore);
        TestCase::assertInstanceOf(
            PDO::class,
            $pdo,
        );

        $pdo->setAttribute(PDO::ATTR_ERRMODE, $pdoErrMode);
        if ($pdo->inTransaction()) {
            $pdo->rollBack();
        }

        if ($invalidTables) {
            $uidTableProperty->setValue($eventStore, $uidTableProperty->getValue($eventStore) . '__MISSING');
            $eventTableProperty->setValue($eventStore, $eventTableProperty->getValue($eventStore) . '__MISSING');
        }
    }

    public static function assertPdoEventStoreErrorMode(PdoEventStore $eventStore, int $expectedPdoErrMode): void
    {
        $reflection = new ReflectionClass($eventStore);
        $pdoProperty = $reflection->getProperty('pdo');
        $pdoProperty->setAccessible(true);

        $pdo = $pdoProperty->getValue($eventStore);
        TestCase::assertInstanceOf(
            PDO::class,
            $pdo,
        );

        TestCase::assertSame(
            $expectedPdoErrMode,
            $pdo->getAttribute(PDO::ATTR_ERRMODE),
            'Expected different PDO error mode',
        );
    }

    public static function beginPdoEventStoreTransaction(PdoEventStore $eventStore): void
    {
        $reflection = new ReflectionClass($eventStore);
        $pdoProperty = $reflection->getProperty('pdo');
        $pdoProperty->setAccessible(true);

        $pdo = $pdoProperty->getValue($eventStore);
        TestCase::assertInstanceOf(
            PDO::class,
            $pdo,
        );

        $pdo->beginTransaction();
    }

    public static function assertPdoEventStoreTransaction(
        PdoEventStore $eventStore,
        bool $expectedActiveTransaction
    ): void {
        $reflection = new ReflectionClass($eventStore);
        $pdoProperty = $reflection->getProperty('pdo');
        $pdoProperty->setAccessible(true);

        $pdo = $pdoProperty->getValue($eventStore);
        TestCase::assertInstanceOf(
            PDO::class,
            $pdo,
        );

        TestCase::assertSame(
            $expectedActiveTransaction,
            $pdo->inTransaction(),
            'Expected different PDO transaction status',
        );
    }

    /**
     * @param UidStreamInterface $actualStream
     * @param string[] $expectedStreamUids
     */
    public static function assertUidStreamMatches(
        UidStreamInterface $actualStream,
        array $expectedStreamUids
    ): void {
        $originalCount = count($expectedStreamUids);
        $expectedStreamUids = array_values(array_unique($expectedStreamUids));

        TestCase::assertCount(
            $originalCount,
            $expectedStreamUids,
        );

        while (null !== $actualStreamUid = $actualStream->next()) {
            TestCase::assertContains(
                $actualStreamUid->toString(),
                $expectedStreamUids,
                'List of expected stream uids did not contain actual stream uid',
            );

            unset($expectedStreamUids[array_search($actualStreamUid->toString(), $expectedStreamUids, true)]);
        }

        TestCase::assertCount(
            0,
            $expectedStreamUids,
        );
    }

    /**
     * $expectedEventDetails = [
     *     [ "StreamUid", Number, "Type", "Data" ],
     * ];
     *
     * @param EventStreamInterface $actualStream
     * @param array<array{string, int, string, string}> $expectedEventDetails
     */
    public static function assertEventStreamMatches(
        EventStreamInterface $actualStream,
        array $expectedEventDetails
    ): void {
        self::assertEventStreamBeginsWith($actualStream, $expectedEventDetails);

        TestCase::assertNull(
            $actualStream->next(),
        );
    }

    /**
     * $expectedEventDetails = [
     *     [ "StreamUid", Number, "Type", "Data" ],
     * ];
     *
     * @param EventStreamInterface $actualStream
     * @param array<array{string, int, string, string}> $expectedEventDetails
     * @return EventReference|null
     */
    public static function assertEventStreamBeginsWith(
        EventStreamInterface $actualStream,
        array $expectedEventDetails
    ): ?EventReference {
        while (null !== $expectedDetails = array_shift($expectedEventDetails)) {
            $actualEvent = $actualStream->next();

            TestCase::assertNotNull(
                $actualEvent,
            );

            TestCase::assertSame(
                $expectedDetails[0],
                $actualEvent->streamUid()->toString(),
            );

            TestCase::assertSame(
                $expectedDetails[1],
                $actualEvent->eventNumber()->toInt(),
            );

            TestCase::assertSame(
                $expectedDetails[2],
                $actualEvent->eventType()->toString(),
            );

            TestCase::assertSame(
                $expectedDetails[3],
                $actualEvent->eventData()->toString(),
            );
        }

        return $actualStream->reference();
    }
}
