<?php

declare(strict_types=1);

namespace BjoernGoetschke\Test\EventStore;

use BjoernGoetschke\EventStore\EventStoreInterface;
use Closure;

final class TestEventStoreFactory implements TestEventStoreFactoryInterface
{
    /**
     * @var Closure(): EventStoreInterface
     */
    private Closure $factory;

    /**
     * @param Closure(): EventStoreInterface $factory
     */
    public function __construct(Closure $factory)
    {
        $this->factory = $factory;
    }

    public function build(): EventStoreInterface
    {
        return ($this->factory)();
    }
}
