<?php

declare(strict_types=1);

namespace BjoernGoetschke\EventStore;

use RuntimeException;

/**
 * Exception that indicates a general error during the processing of the event store methods.
 *
 * @api usage
 * @since 1.0
 * @copyright BSD-2-Clause, see LICENSE.txt and README.md files provided with the library source code
 */
final class EventStoreRuntimeException extends RuntimeException implements EventStoreRuntimeExceptionInterface
{
}
