<?php

declare(strict_types=1);

namespace BjoernGoetschke\EventStore;

use BadMethodCallException;
use InvalidArgumentException;

/**
 * Holds the reference to a specific event in the event store, must not be longer than 128 bytes.
 *
 * The format of the reference is implementation specific.
 *
 * @api usage
 * @since 1.0
 * @copyright BSD-2-Clause, see LICENSE.txt and README.md files provided with the library source code
 */
final class EventReference
{
    /**
     * The event reference.
     */
    private string $reference;

    /**
     * Constructor.
     *
     * @param string $reference
     *        Event reference, must not be longer than 128 bytes.
     * @throws InvalidArgumentException
     * @no-named-arguments
     */
    public function __construct(string $reference)
    {
        $this->reference = $reference;
        $numberOfBytes = strlen($this->reference);
        if ($numberOfBytes < 1 || $numberOfBytes > 128) {
            $msg = sprintf(
                'Stream uid must not be empty and not longer than 128 bytes: (%1$d) %2$s',
                $numberOfBytes,
                $this->reference,
            );
            throw new InvalidArgumentException($msg);
        }
    }

    /**
     * Prevent clone.
     *
     * @codeCoverageIgnore
     */
    private function __clone()
    {
    }

    /**
     * Prevent serialize.
     *
     * @return array<string, mixed>
     * @codeCoverageIgnore
     */
    public function __serialize(): array
    {
        throw new BadMethodCallException('Cannot serialize ' . __CLASS__);
    }

    /**
     * Prevent unserialize.
     *
     * @param array<string, mixed> $data
     * @codeCoverageIgnore
     */
    public function __unserialize(array $data): void
    {
        throw new BadMethodCallException('Cannot unserialize ' . __CLASS__);
    }

    /**
     * Returns the event reference as string.
     *
     * @return string
     * @api usage
     * @since 1.0
     */
    public function toString(): string
    {
        return $this->reference;
    }

    /**
     * Returns the event reference as string.
     *
     * @return string
     * @since 1.0
     */
    public function __toString(): string
    {
        return $this->toString();
    }
}
