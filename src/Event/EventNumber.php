<?php

declare(strict_types=1);

namespace BjoernGoetschke\EventStore\Event;

use BadMethodCallException;
use InvalidArgumentException;

/**
 * Holds the number of an event as integer, must be greater than 0.
 *
 * @api usage
 * @since 1.0
 * @copyright BSD-2-Clause, see LICENSE.txt and README.md files provided with the library source code
 */
final class EventNumber
{
    /**
     * The event number.
     */
    private int $eventNumber;

    /**
     * Constructor.
     *
     * @param int $number
     *        The event number, must be greater than 0.
     * @throws InvalidArgumentException
     * @no-named-arguments
     */
    public function __construct(int $number)
    {
        $this->eventNumber = $number;
        if ($this->eventNumber < 1) {
            $msg = sprintf(
                'Event number must be at least one: %1$d',
                $this->eventNumber,
            );
            throw new InvalidArgumentException($msg);
        }
    }

    /**
     * Prevent clone.
     *
     * @codeCoverageIgnore
     */
    private function __clone()
    {
    }

    /**
     * Prevent serialize.
     *
     * @return array<string, mixed>
     * @codeCoverageIgnore
     */
    public function __serialize(): array
    {
        throw new BadMethodCallException('Cannot serialize ' . __CLASS__);
    }

    /**
     * Prevent unserialize.
     *
     * @param array<string, mixed> $data
     * @codeCoverageIgnore
     */
    public function __unserialize(array $data): void
    {
        throw new BadMethodCallException('Cannot unserialize ' . __CLASS__);
    }

    /**
     * Returns the event number as integer.
     *
     * @return int
     * @api usage
     * @since 1.0
     */
    public function toInt(): int
    {
        return $this->eventNumber;
    }

    /**
     * Returns the event number as string.
     *
     * @return string
     * @api usage
     * @since 1.0
     */
    public function toString(): string
    {
        return (string)$this->toInt();
    }

    /**
     * Returns the event number as string.
     *
     * @return string
     * @since 1.0
     */
    public function __toString(): string
    {
        return $this->toString();
    }
}
