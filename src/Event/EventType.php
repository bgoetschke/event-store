<?php

declare(strict_types=1);

namespace BjoernGoetschke\EventStore\Event;

use BadMethodCallException;
use InvalidArgumentException;

/**
 * Holds the type of event as string, must not be longer than 128 bytes.
 *
 * @api usage
 * @since 1.0
 * @copyright BSD-2-Clause, see LICENSE.txt and README.md files provided with the library source code
 */
final class EventType
{
    /**
     * The event type.
     */
    private string $eventType;

    /**
     * Constructor.
     *
     * @param string $type
     *        The event type, must not be longer than 128 bytes.
     * @throws InvalidArgumentException
     * @no-named-arguments
     */
    public function __construct(string $type)
    {
        $this->eventType = $type;
        $numberOfBytes = strlen($this->eventType);
        if ($numberOfBytes < 1 || $numberOfBytes > 128) {
            $msg = sprintf(
                'Event type must not be empty and not longer than 128 bytes: (%1$d) %2$s',
                $numberOfBytes,
                $this->eventType,
            );
            throw new InvalidArgumentException($msg);
        }
    }

    /**
     * Prevent clone.
     *
     * @codeCoverageIgnore
     */
    private function __clone()
    {
    }

    /**
     * Prevent serialize.
     *
     * @return array<string, mixed>
     * @codeCoverageIgnore
     */
    public function __serialize(): array
    {
        throw new BadMethodCallException('Cannot serialize ' . __CLASS__);
    }

    /**
     * Prevent unserialize.
     *
     * @param array<string, mixed> $data
     * @codeCoverageIgnore
     */
    public function __unserialize(array $data): void
    {
        throw new BadMethodCallException('Cannot unserialize ' . __CLASS__);
    }

    /**
     * Returns the event type as string.
     *
     * @return string
     * @api usage
     * @since 1.0
     */
    public function toString(): string
    {
        return $this->eventType;
    }

    /**
     * Returns the event type as string.
     *
     * @return string
     * @since 1.0
     */
    public function __toString(): string
    {
        return $this->toString();
    }
}
