<?php

declare(strict_types=1);

namespace BjoernGoetschke\EventStore\Event;

use BadMethodCallException;
use BjoernGoetschke\EventStore\StreamUid;
use InvalidArgumentException;

/**
 * Represents a single event of an event stream.
 *
 * @api usage
 * @since 1.0
 * @copyright BSD-2-Clause, see LICENSE.txt and README.md files provided with the library source code
 */
final class StreamEvent
{
    /**
     * The stream uid the event belongs to.
     */
    private StreamUid $streamUid;

    /**
     * The number of the event.
     */
    private EventNumber $eventNumber;

    /**
     * The type of the event.
     */
    private EventType $eventType;

    /**
     * The data of the event.
     */
    private EventData $eventData;

    /**
     * Constructor.
     *
     * @param StreamUid $streamUid
     *        The stream uid the event belongs to.
     * @param EventNumber $number
     *        The number of the event.
     * @param EventType $type
     *        The type of the event.
     * @param EventData $data
     *        The data of the event.
     * @no-named-arguments
     */
    public function __construct(StreamUid $streamUid, EventNumber $number, EventType $type, EventData $data)
    {
        $this->streamUid = $streamUid;
        $this->eventNumber = $number;
        $this->eventType = $type;
        $this->eventData = $data;
    }

    /**
     * Creates a new event from the specified arguments.
     *
     * Simply wraps the arguments in the required data type classes.
     *
     * @param string $streamUid
     *        Stream uid, must not be longer than 128 bytes {@see StreamUid}.
     * @param int $number
     *        The event number, must be greater than 0 {@see EventNumber}.
     * @param string $type
     *        The event type, must not be longer than 128 bytes {@see EventType}.
     * @param string $data
     *        The event data {@see EventData}.
     * @return self
     * @throws InvalidArgumentException
     * @no-named-arguments
     * @api usage
     * @since 1.0
     */
    public static function fromBasicTypes(string $streamUid, int $number, string $type, string $data): self
    {
        return new self(
            new StreamUid($streamUid),
            new EventNumber($number),
            new EventType($type),
            new EventData($data),
        );
    }

    /**
     * Prevent clone.
     *
     * @codeCoverageIgnore
     */
    private function __clone()
    {
    }

    /**
     * Prevent serialize.
     *
     * @return array<string, mixed>
     * @codeCoverageIgnore
     */
    public function __serialize(): array
    {
        throw new BadMethodCallException('Cannot serialize ' . __CLASS__);
    }

    /**
     * Prevent unserialize.
     *
     * @param array<string, mixed> $data
     * @codeCoverageIgnore
     */
    public function __unserialize(array $data): void
    {
        throw new BadMethodCallException('Cannot unserialize ' . __CLASS__);
    }

    /**
     * Returns the stream uid the event belongs to.
     *
     * @return StreamUid
     * @api usage
     * @since 1.0
     */
    public function streamUid(): StreamUid
    {
        return $this->streamUid;
    }

    /**
     * Returns the event number.
     *
     * @return EventNumber
     * @api usage
     * @since 1.0
     */
    public function eventNumber(): EventNumber
    {
        return $this->eventNumber;
    }

    /**
     * Returns the event type.
     *
     * @return EventType
     * @api usage
     * @since 1.0
     */
    public function eventType(): EventType
    {
        return $this->eventType;
    }

    /**
     * Returns the event data.
     *
     * @return EventData
     * @api usage
     * @since 1.0
     */
    public function eventData(): EventData
    {
        return $this->eventData;
    }
}
