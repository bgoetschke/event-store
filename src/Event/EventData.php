<?php

declare(strict_types=1);

namespace BjoernGoetschke\EventStore\Event;

use BadMethodCallException;

/**
 * Holds the binary data of an event as string.
 *
 * @api usage
 * @since 1.0
 * @copyright BSD-2-Clause, see LICENSE.txt and README.md files provided with the library source code
 */
final class EventData
{
    /**
     * The event data.
     */
    private string $eventData;

    /**
     * Constructor.
     *
     * @param string $data
     *        The event data.
     * @no-named-arguments
     */
    public function __construct(string $data)
    {
        $this->eventData = $data;
    }

    /**
     * Prevent clone.
     *
     * @codeCoverageIgnore
     */
    private function __clone()
    {
    }

    /**
     * Prevent serialize.
     *
     * @return array<string, mixed>
     * @codeCoverageIgnore
     */
    public function __serialize(): array
    {
        throw new BadMethodCallException('Cannot serialize ' . __CLASS__);
    }

    /**
     * Prevent unserialize.
     *
     * @param array<string, mixed> $data
     * @codeCoverageIgnore
     */
    public function __unserialize(array $data): void
    {
        throw new BadMethodCallException('Cannot unserialize ' . __CLASS__);
    }

    /**
     * Returns the event data as string.
     *
     * @return string
     * @api usage
     * @since 1.0
     */
    public function toString(): string
    {
        return $this->eventData;
    }

    /**
     * Returns the event data as string.
     *
     * @return string
     * @since 1.0
     */
    public function __toString(): string
    {
        return $this->toString();
    }
}
