<?php

declare(strict_types=1);

namespace BjoernGoetschke\EventStore;

use Throwable;

/**
 * Exception interface that indicates a general error during the processing of the event store methods.
 *
 * @api stable
 * @since 2.0
 * @copyright BSD-2-Clause, see LICENSE.txt and README.md files provided with the library source code
 */
interface EventStoreRuntimeExceptionInterface extends Throwable
{
}
