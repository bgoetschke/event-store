<?php

declare(strict_types=1);

namespace BjoernGoetschke\EventStore;

use BjoernGoetschke\EventStore\Event\EventNumber;
use BjoernGoetschke\EventStore\Event\EventType;
use BjoernGoetschke\EventStore\Event\StreamEvent;
use BjoernGoetschke\EventStore\Stream\EventStreamInterface;
use BjoernGoetschke\EventStore\Stream\UidStreamInterface;

/**
 * Event store interface.
 *
 * @api stable
 * @since 1.0
 * @copyright BSD-2-Clause, see LICENSE.txt and README.md files provided with the library source code
 */
interface EventStoreInterface
{
    /**
     * Returns a stream of {@see StreamUid} instances that exist in the event store.
     *
     * @return UidStreamInterface
     * @throws EventStoreRuntimeExceptionInterface
     * @api stable
     * @since 1.0
     */
    public function streams(): UidStreamInterface;

    /**
     * Returns the highest {@see EventNumber} of the specified stream or null if the stream does not exist or does
     * not contain any events.
     *
     * @param StreamUid $uid
     *        The uid of the stream to return the position of.
     * @return EventNumber|null
     * @throws EventStoreRuntimeExceptionInterface
     * @no-named-arguments
     * @api stable
     * @since 1.0
     */
    public function position(StreamUid $uid): ?EventNumber;

    /**
     * Returns a stream of {@see StreamEvent} instances from the specified stream.
     *
     * @param StreamUid $uid
     *        The uid of the stream to return the events of.
     * @param EventNumber|null $beginAfter
     *        If specified, only return events after the specified event number. In case the stream does not contain
     *        the event with the specified number yet the returned stream will not contain any events.
     * @return EventStreamInterface
     * @throws EventStoreRuntimeExceptionInterface
     * @no-named-arguments
     * @api stable
     * @since 1.0
     */
    public function streamEvents(StreamUid $uid, ?EventNumber $beginAfter = null): EventStreamInterface;

    /**
     * Returns a stream of {@see StreamEvent} instances from all streams.
     *
     * @param EventType[] $filterEventTypes
     *        If not empty, only return events of the specified types.
     * @param EventReference|null $beginAfter
     *        If specified, only return events after the specified event reference. In case the reference does
     *        not exist or is invalid an exception will be thrown.
     * @return EventStreamInterface
     * @throws EventStoreRuntimeExceptionInterface
     * @no-named-arguments
     * @api stable
     * @since 1.0
     */
    public function allEvents(array $filterEventTypes = [], ?EventReference $beginAfter = null): EventStreamInterface;

    /**
     * Stores the list of events in the event store.
     *
     * @param StreamEvent[] $events
     *        The list of events that should be written.
     * @throws EventStoreRuntimeExceptionInterface
     * @throws EventStoreConcurrencyExceptionInterface
     * @no-named-arguments
     * @api stable
     * @since 1.0
     */
    public function write(array $events): void;
}
