<?php

declare(strict_types=1);

namespace BjoernGoetschke\EventStore;

/**
 * Exception interface that indicates that the event store has been modified by another process and a conflict occurred,
 * the calling application can refresh the data from the event store and then retry the operation.
 *
 * @api stable
 * @since 2.0
 * @copyright BSD-2-Clause, see LICENSE.txt and README.md files provided with the library source code
 */
interface EventStoreConcurrencyExceptionInterface extends EventStoreRuntimeExceptionInterface
{
}
