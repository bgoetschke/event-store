<?php

declare(strict_types=1);

namespace BjoernGoetschke\EventStore\Stream;

use BadMethodCallException;
use BjoernGoetschke\EventStore\StreamUid;
use Iterator;

/**
 * Returns {@see StreamUid} instances provided by an {@see Iterator}.
 *
 * @api usage
 * @since 1.0
 * @copyright BSD-2-Clause, see LICENSE.txt and README.md files provided with the library source code
 */
final class IteratorUidStream implements UidStreamInterface
{
    /**
     * The iterator that provides the stream uids.
     *
     * @var Iterator<int, StreamUid>
     */
    private Iterator $iterator;

    /**
     * Constructor.
     *
     * @param Iterator<int, StreamUid> $iterator
     *        The iterator that provides the stream uids.
     * @no-named-arguments
     */
    public function __construct(Iterator $iterator)
    {
        $this->iterator = $iterator;
    }

    /**
     * Prevent clone.
     *
     * @codeCoverageIgnore
     */
    private function __clone()
    {
    }

    /**
     * Prevent serialize.
     *
     * @return array<string, mixed>
     * @codeCoverageIgnore
     */
    public function __serialize(): array
    {
        throw new BadMethodCallException('Cannot serialize ' . __CLASS__);
    }

    /**
     * Prevent unserialize.
     *
     * @param array<string, mixed> $data
     * @codeCoverageIgnore
     */
    public function __unserialize(array $data): void
    {
        throw new BadMethodCallException('Cannot unserialize ' . __CLASS__);
    }

    public function next(): ?StreamUid
    {
        if (!$this->iterator->valid()) {
            return null;
        }

        $uid = $this->iterator->current();
        $this->iterator->next();
        return $uid;
    }
}
