<?php

declare(strict_types=1);

namespace BjoernGoetschke\EventStore\Stream;

use BjoernGoetschke\EventStore\Event\StreamEvent;
use BjoernGoetschke\EventStore\EventReference;

/**
 * A stream of {@see StreamEvent} instances.
 *
 * @api stable
 * @since 1.0
 * @copyright BSD-2-Clause, see LICENSE.txt and README.md files provided with the library source code
 */
interface EventStreamInterface
{
    /**
     * Returns the reference to the last {@see StreamEvent} that has been returned by {@see next()}
     * or null in case no event has been returned yet.
     *
     * @return EventReference|null
     * @api stable
     * @since 1.0
     */
    public function reference(): ?EventReference;

    /**
     * Returns the next {@see StreamEvent} instance of the stream or null when the end of the stream is reached.
     *
     * @return StreamEvent|null
     * @api stable
     * @since 1.0
     */
    public function next(): ?StreamEvent;
}
