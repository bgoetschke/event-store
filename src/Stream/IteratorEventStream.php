<?php

declare(strict_types=1);

namespace BjoernGoetschke\EventStore\Stream;

use BadMethodCallException;
use BjoernGoetschke\EventStore\Event\StreamEvent;
use BjoernGoetschke\EventStore\EventReference;
use Iterator;

/**
 * Returns {@see StreamEvent} instances for a specific stream provided by an {@see Iterator}.
 *
 * @api usage
 * @since 1.0
 * @copyright BSD-2-Clause, see LICENSE.txt and README.md files provided with the library source code
 */
final class IteratorEventStream implements EventStreamInterface
{
    /**
     * The iterator that provides the stream events.
     *
     * @var Iterator<int, EventStreamEntry>
     */
    private Iterator $iterator;

    /**
     * The reference to the last event returned by {@see next()}.
     */
    private ?EventReference $reference = null;

    /**
     * Constructor.
     *
     * @param Iterator<int, EventStreamEntry> $iterator
     *        The iterator that provides the stream events.
     * @no-named-arguments
     */
    public function __construct(Iterator $iterator)
    {
        $this->iterator = $iterator;
    }

    /**
     * Prevent clone.
     *
     * @codeCoverageIgnore
     */
    private function __clone()
    {
    }

    /**
     * Prevent serialize.
     *
     * @return array<string, mixed>
     * @codeCoverageIgnore
     */
    public function __serialize(): array
    {
        throw new BadMethodCallException('Cannot serialize ' . __CLASS__);
    }

    /**
     * Prevent unserialize.
     *
     * @param array<string, mixed> $data
     * @codeCoverageIgnore
     */
    public function __unserialize(array $data): void
    {
        throw new BadMethodCallException('Cannot unserialize ' . __CLASS__);
    }

    public function reference(): ?EventReference
    {
        return $this->reference;
    }

    public function next(): ?StreamEvent
    {
        if (!$this->iterator->valid()) {
            return null;
        }

        $event = $this->handleEntry($this->iterator->current());
        $this->iterator->next();
        return $event;
    }

    /**
     * @param EventStreamEntry $entry
     * @return StreamEvent
     * @no-named-arguments
     */
    private function handleEntry(EventStreamEntry $entry): StreamEvent
    {
        $this->reference = $entry->reference();
        return $entry->event();
    }
}
