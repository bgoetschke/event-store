<?php

declare(strict_types=1);

namespace BjoernGoetschke\EventStore\Stream;

use BadMethodCallException;
use BjoernGoetschke\EventStore\Event\StreamEvent;
use BjoernGoetschke\EventStore\EventReference;
use InvalidArgumentException;

/**
 * Represents a single entry of an event stream.
 *
 * @api usage
 * @since 1.0
 * @copyright BSD-2-Clause, see LICENSE.txt and README.md files provided with the library source code
 */
final class EventStreamEntry
{
    /**
     * The reference to the event.
     */
    private EventReference $reference;

    /**
     * The event that the entry represents.
     */
    private StreamEvent $event;

    /**
     * Constructor.
     *
     * @param EventReference $reference
     *        The reference to the event.
     * @param StreamEvent $event
     *        The event that the entry represents.
     * @no-named-arguments
     */
    public function __construct(EventReference $reference, StreamEvent $event)
    {
        $this->reference = $reference;
        $this->event = $event;
    }

    /**
     * Creates a new entry from the specified arguments.
     *
     * Simply wraps the arguments in the required data type classes.
     *
     * @param string $reference
     *        Event reference, must not be longer than 128 bytes {@see EventReference}.
     * @param string $streamUid
     *        Stream uid, must not be longer than 128 bytes {@see StreamUid}.
     * @param int $number
     *        The event number, must be greater than 0 {@see EventNumber}.
     * @param string $type
     *        The event type, must not be longer than 128 bytes {@see EventType}.
     * @param string $data
     *        The event data {@see EventData}.
     * @return self
     * @throws InvalidArgumentException
     * @no-named-arguments
     * @api usage
     * @since 1.0
     */
    public static function fromBasicTypes(
        string $reference,
        string $streamUid,
        int $number,
        string $type,
        string $data
    ): self {
        return new self(
            new EventReference($reference),
            StreamEvent::fromBasicTypes($streamUid, $number, $type, $data),
        );
    }

    /**
     * Prevent clone.
     *
     * @codeCoverageIgnore
     */
    private function __clone()
    {
    }

    /**
     * Prevent serialize.
     *
     * @return array<string, mixed>
     * @codeCoverageIgnore
     */
    public function __serialize(): array
    {
        throw new BadMethodCallException('Cannot serialize ' . __CLASS__);
    }

    /**
     * Prevent unserialize.
     *
     * @param array<string, mixed> $data
     * @codeCoverageIgnore
     */
    public function __unserialize(array $data): void
    {
        throw new BadMethodCallException('Cannot unserialize ' . __CLASS__);
    }

    /**
     * Returns the reference to the event.
     *
     * @return EventReference
     * @api usage
     * @since 1.0
     */
    public function reference(): EventReference
    {
        return $this->reference;
    }

    /**
     * Returns the event that the entry represents.
     *
     * @return StreamEvent
     * @api usage
     * @since 1.0
     */
    public function event(): StreamEvent
    {
        return $this->event;
    }
}
