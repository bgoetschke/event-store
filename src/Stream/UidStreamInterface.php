<?php

declare(strict_types=1);

namespace BjoernGoetschke\EventStore\Stream;

use BjoernGoetschke\EventStore\StreamUid;

/**
 * A stream of {@see StreamUid} instances.
 *
 * @api stable
 * @since 1.0
 * @copyright BSD-2-Clause, see LICENSE.txt and README.md files provided with the library source code
 */
interface UidStreamInterface
{
    /**
     * Returns the next {@see StreamUid} instance of the stream or null when the end of the stream is reached.
     *
     * @return StreamUid|null
     * @api stable
     * @since 1.0
     */
    public function next(): ?StreamUid;
}
