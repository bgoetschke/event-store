<?php

declare(strict_types=1);

namespace BjoernGoetschke\EventStore;

use BadMethodCallException;
use InvalidArgumentException;

/**
 * Holds the uid of a stream as string, must not be longer than 128 bytes.
 *
 * @api usage
 * @since 1.0
 * @copyright BSD-2-Clause, see LICENSE.txt and README.md files provided with the library source code
 */
final class StreamUid
{
    /**
     * The stream uid.
     */
    private string $uid;

    /**
     * Constructor.
     *
     * @param string $uid
     *        Stream uid, must not be longer than 128 bytes.
     * @throws InvalidArgumentException
     * @no-named-arguments
     */
    public function __construct(string $uid)
    {
        $this->uid = $uid;
        $numberOfBytes = strlen($this->uid);
        if ($numberOfBytes < 1 || $numberOfBytes > 128) {
            $msg = sprintf(
                'Stream uid must not be empty and not longer than 128 bytes: (%1$d) %2$s',
                $numberOfBytes,
                $this->uid,
            );
            throw new InvalidArgumentException($msg);
        }
    }

    /**
     * Prevent clone.
     *
     * @codeCoverageIgnore
     */
    private function __clone()
    {
    }

    /**
     * Prevent serialize.
     *
     * @return array<string, mixed>
     * @codeCoverageIgnore
     */
    public function __serialize(): array
    {
        throw new BadMethodCallException('Cannot serialize ' . __CLASS__);
    }

    /**
     * Prevent unserialize.
     *
     * @param array<string, mixed> $data
     * @codeCoverageIgnore
     */
    public function __unserialize(array $data): void
    {
        throw new BadMethodCallException('Cannot unserialize ' . __CLASS__);
    }

    /**
     * Returns the stream uid as string.
     *
     * @return string
     * @api usage
     * @since 1.0
     */
    public function toString(): string
    {
        return $this->uid;
    }

    /**
     * Returns the stream uid as string.
     *
     * @return string
     * @since 1.0
     */
    public function __toString(): string
    {
        return $this->toString();
    }
}
