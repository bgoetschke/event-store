<?php

declare(strict_types=1);

namespace BjoernGoetschke\EventStore;

use RuntimeException;

/**
 * Exception that indicates that the event store has been modified by another process and a conflict occurred,
 * the calling application can refresh the data from the event store and then retry the operation.
 *
 * @api usage
 * @since 1.0
 * @copyright BSD-2-Clause, see LICENSE.txt and README.md files provided with the library source code
 */
final class EventStoreConcurrencyException extends RuntimeException implements EventStoreConcurrencyExceptionInterface
{
}
