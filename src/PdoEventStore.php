<?php

declare(strict_types=1);

namespace BjoernGoetschke\EventStore;

use BadMethodCallException;
use BjoernGoetschke\EventStore\Event\EventNumber;
use BjoernGoetschke\EventStore\Event\EventType;
use BjoernGoetschke\EventStore\Event\StreamEvent;
use BjoernGoetschke\EventStore\Stream\EventStreamEntry;
use BjoernGoetschke\EventStore\Stream\EventStreamInterface;
use BjoernGoetschke\EventStore\Stream\IteratorEventStream;
use BjoernGoetschke\EventStore\Stream\IteratorUidStream;
use BjoernGoetschke\EventStore\Stream\UidStreamInterface;
use PDO;
use PDOException;

/**
 * Event store that uses a {@see PDO} instance to store events in a SQL database.
 *
 * @api usage
 * @since 1.0
 * @copyright BSD-2-Clause, see LICENSE.txt and README.md files provided with the library source code
 */
final class PdoEventStore implements EventStoreInterface
{
    /**
     * The used PDO instance.
     */
    private PDO $pdo;

    /**
     * The used database table to store stream uids.
     */
    private string $uidTable;

    /**
     * The used database table to store stream events.
     */
    private string $eventTable;

    /**
     * The number of uids that is fetched for an uid stream at once.
     */
    private int $uidChunkSize;

    /**
     * The number of events that is fetched for an event stream at once.
     */
    private int $eventChunkSize;

    /**
     * Constructor.
     *
     * @param PDO $pdo
     *        The PDO instance that should be used.
     * @param string $uidTable
     *        The name of the database table to store stream uids.
     * @param string $eventTable
     *        The name of the database table to store stream events.
     * @param int $uidChunkSize
     *        The number of uids that is fetched for an uid stream at once, at least 1.
     * @param int $eventChunkSize
     *        The number of events that is fetched for an event stream at once, at least 1.
     * @no-named-arguments
     */
    public function __construct(
        PDO $pdo,
        string $uidTable,
        string $eventTable,
        int $uidChunkSize = 1000,
        int $eventChunkSize = 100
    ) {
        $this->pdo = $pdo;
        $this->uidTable = $uidTable;
        $this->eventTable = $eventTable;
        $this->uidChunkSize = max([1, $uidChunkSize]);
        $this->eventChunkSize = max([1, $eventChunkSize]);
    }

    /**
     * Prevent clone.
     *
     * @codeCoverageIgnore
     */
    private function __clone()
    {
    }

    /**
     * Prevent serialize.
     *
     * @return array<string, mixed>
     * @codeCoverageIgnore
     */
    public function __serialize(): array
    {
        throw new BadMethodCallException('Cannot serialize ' . __CLASS__);
    }

    /**
     * Prevent unserialize.
     *
     * @param array<string, mixed> $data
     * @codeCoverageIgnore
     */
    public function __unserialize(array $data): void
    {
        throw new BadMethodCallException('Cannot unserialize ' . __CLASS__);
    }

    public function streams(): UidStreamInterface
    {
        $errorMode = null;
        try {
            $errorMode = $this->setPdoErrorMode();
            return $this->readStreams();
        } catch (PDOException $e) {
            throw new EventStoreRuntimeException('Event store operation failed.', 0, $e);
        } finally {
            $this->restorePdoErrorMode($errorMode);
        }
    }

    /**
     * @return UidStreamInterface
     * @throws PDOException
     */
    private function readStreams(): UidStreamInterface
    {
        $offset = 0;

        $statement = $this->pdo->prepare(
            'SELECT
                `stream`,
                `uid`
            FROM
                `' . $this->uidTable . '`
            WHERE
                `stream` > :offset
            ORDER BY
                `stream` ASC
            LIMIT
                ' . $this->uidChunkSize,
        );
        $statement->bindValue('offset', $offset, PDO::PARAM_INT);
        $statement->execute();
        $rows = $statement->fetchAll(PDO::FETCH_OBJ);
        assert($rows !== false);
        $statement->closeCursor();

        $generator = function () use ($statement, $offset, $rows) {
            do {
                foreach ($rows as $row) {
                    $offset = (int)$row->stream;
                    yield new StreamUid($row->uid);
                }
                $statement->bindValue('offset', $offset, PDO::PARAM_INT);
                $statement->execute();
                $rows = $statement->fetchAll(PDO::FETCH_OBJ);
                assert($rows !== false);
                $statement->closeCursor();
            } while (count($rows) > 0);
        };

        return new IteratorUidStream($generator());
    }

    public function position(StreamUid $uid): ?EventNumber
    {
        $errorMode = null;
        try {
            $errorMode = $this->setPdoErrorMode();
            return $this->readPosition($uid);
        } catch (PDOException $e) {
            throw new EventStoreRuntimeException('Event store operation failed.', 0, $e);
        } finally {
            $this->restorePdoErrorMode($errorMode);
        }
    }

    /**
     * @param StreamUid $uid
     * @return EventNumber|null
     * @throws PDOException
     * @no-named-arguments
     */
    private function readPosition(StreamUid $uid): ?EventNumber
    {
        $number = $this->maxNumber($this->streamIndex($uid, false));
        if ($number > 0) {
            return new EventNumber($number);
        }
        return null;
    }

    public function streamEvents(StreamUid $uid, ?EventNumber $beginAfter = null): EventStreamInterface
    {
        $errorMode = null;
        try {
            $errorMode = $this->setPdoErrorMode();
            return $this->readStreamEvents($uid, $beginAfter);
        } catch (PDOException $e) {
            throw new EventStoreRuntimeException('Event store operation failed.', 0, $e);
        } finally {
            $this->restorePdoErrorMode($errorMode);
        }
    }

    /**
     * @param StreamUid $uid
     * @param EventNumber|null $beginAfter
     * @return EventStreamInterface
     * @throws PDOException
     * @no-named-arguments
     */
    private function readStreamEvents(StreamUid $uid, ?EventNumber $beginAfter): EventStreamInterface
    {
        $streamIndex = $this->streamIndex($uid, false);

        $number = 0;
        if ($beginAfter !== null) {
            $number = $beginAfter->toInt();
        }

        $statement = $this->pdo->prepare(
            'SELECT
                `id`,
                `number`,
                `type`,
                `data`
            FROM
                `' . $this->eventTable . '`
            WHERE
                `stream` = :stream AND
                `number` > :number
            ORDER BY
                `number` ASC
            LIMIT
                ' . $this->eventChunkSize,
        );
        $statement->bindValue('stream', $streamIndex, PDO::PARAM_INT);
        $statement->bindValue('number', $number, PDO::PARAM_INT);
        $statement->execute();
        $rows = $statement->fetchAll(PDO::FETCH_OBJ);
        assert($rows !== false);
        $statement->closeCursor();

        $generator = function () use ($uid, $statement, $number, $rows) {
            do {
                foreach ($rows as $row) {
                    $number = (int)$row->number;
                    yield EventStreamEntry::fromBasicTypes(
                        (string)$row->id,
                        $uid->toString(),
                        $number,
                        (string)$row->type,
                        (string)$row->data,
                    );
                }
                $statement->bindValue('number', $number, PDO::PARAM_INT);
                $statement->execute();
                $rows = $statement->fetchAll(PDO::FETCH_OBJ);
                assert($rows !== false);
                $statement->closeCursor();
            } while (count($rows) > 0);
        };

        return new IteratorEventStream($generator());
    }

    public function allEvents(array $filterEventTypes = [], ?EventReference $beginAfter = null): EventStreamInterface
    {
        $filterEventTypes = array_map(
            function (EventType $type) {
                return $type->toString();
            },
            $filterEventTypes,
        );
        $filterEventTypes = array_values(array_unique($filterEventTypes));
        $filterEventKeys = array_map(
            function ($key) {
                return 'type_' . $key;
            },
            array_keys($filterEventTypes),
        );
        $filterEventKeys = array_values($filterEventKeys);
        $filterTypes = array_combine($filterEventKeys, $filterEventTypes);
        unset($filterEventTypes, $filterEventKeys);
        assert($filterTypes !== false);

        $errorMode = null;
        try {
            $errorMode = $this->setPdoErrorMode();
            return $this->readAllEvents($filterTypes, $beginAfter);
        } catch (PDOException $e) {
            throw new EventStoreRuntimeException('Event store operation failed.', 0, $e);
        } finally {
            $this->restorePdoErrorMode($errorMode);
        }
    }

    /**
     * @param string[] $filterTypes
     * @param EventReference|null $beginAfter
     * @return EventStreamInterface
     * @throws PDOException
     * @throws EventStoreRuntimeExceptionInterface
     * @no-named-arguments
     */
    private function readAllEvents(array $filterTypes, ?EventReference $beginAfter): EventStreamInterface
    {
        $filterPlaceholders = array_map(
            function ($key) {
                return ':' . $key;
            },
            array_keys($filterTypes),
        );
        $filterQuery = implode(', ', $filterPlaceholders);
        if ($filterQuery !== '') {
            $filterQuery = 'AND `e`.`type` IN(' . $filterQuery . ')';
        }

        $offset = 0;
        if ($beginAfter !== null) {
            $offset = $this->resolveEventReference($beginAfter);
        }

        $statement = $this->pdo->prepare(
            'SELECT
                `e`.`id`,
                `u`.`uid` `stream`,
                `e`.`number`,
                `e`.`type`,
                `e`.`data`
            FROM
                `' . $this->eventTable . '` `e`
            LEFT JOIN
                `' . $this->uidTable . '` `u`
            ON
                `e`.`stream` = `u`.`stream`
            WHERE
                `e`.`id` > :id
                ' . $filterQuery . '
            ORDER BY
                `e`.`id` ASC
            LIMIT
                ' . $this->eventChunkSize,
        );
        $statement->bindValue('id', $offset, PDO::PARAM_INT);
        foreach ($filterTypes as $key => $value) {
            $statement->bindValue($key, $value, PDO::PARAM_STR);
        }
        $statement->execute();
        $rows = $statement->fetchAll(PDO::FETCH_OBJ);
        assert($rows !== false);
        $statement->closeCursor();

        $generator = function () use ($statement, $offset, $rows) {
            do {
                foreach ($rows as $row) {
                    $offset = (string)$row->id;
                    yield EventStreamEntry::fromBasicTypes(
                        $offset,
                        (string)$row->stream,
                        (int)$row->number,
                        (string)$row->type,
                        (string)$row->data,
                    );
                }
                $statement->bindValue('id', $offset, PDO::PARAM_INT);
                $statement->execute();
                $rows = $statement->fetchAll(PDO::FETCH_OBJ);
                assert($rows !== false);
                $statement->closeCursor();
            } while (count($rows) > 0);
        };

        return new IteratorEventStream($generator());
    }

    public function write(array $events): void
    {
        $events = array_map(
            function (StreamEvent $event) {
                return $event;
            },
            $events,
        );
        $events = array_values($events);

        $errorMode = null;
        $manageTransaction = false;
        try {
            $errorMode = $this->setPdoErrorMode();
            if (!$this->pdo->inTransaction()) {
                $this->pdo->beginTransaction();
                $manageTransaction = true;
            }
            $this->writeEvents($events);
            if ($manageTransaction) {
                $this->pdo->commit();
            }
        } catch (PDOException $e) {
            if ($manageTransaction) {
                $this->pdo->rollBack();
            }
            throw new EventStoreRuntimeException('Event store operation failed.', 0, $e);
        } catch (EventStoreRuntimeExceptionInterface $e) {
            if ($manageTransaction) {
                $this->pdo->rollBack();
            }
            throw $e;
        } finally {
            $this->restorePdoErrorMode($errorMode);
        }
    }

    /**
     * @param StreamEvent[] $events
     * @throws PDOException
     * @throws EventStoreRuntimeExceptionInterface
     * @throws EventStoreConcurrencyExceptionInterface
     * @no-named-arguments
     */
    private function writeEvents(array $events): void
    {
        $insertEventStatement = $this->pdo->prepare(
            'INSERT INTO
                `' . $this->eventTable . '`
            (
                `stream`,
                `number`,
                `type`,
                `data`
            ) VALUES (
                :stream,
                :number,
                :type,
                :data
            )',
        );

        $cachedStreamData = [];
        $isFirstEvent = true;
        foreach ($events as $event) {
            $streamUid = $event->streamUid()->toString();

            if (!array_key_exists($streamUid, $cachedStreamData)) {
                $streamIndex = $this->streamIndex($event->streamUid(), true);
                $max = $this->maxNumber($streamIndex);
                $cachedStreamData[$streamUid] = (object)[
                    'streamIndex' => $streamIndex,
                    'initialNumber' => $max,
                    'expectedNumber' => $max + 1,
                ];
                unset($streamIndex, $max);
            }

            $expectedNumber = $cachedStreamData[$streamUid]->expectedNumber++;
            $actualNumber = $event->eventNumber()->toInt();

            if ($expectedNumber === $actualNumber) {
                $insertEventStatement->bindValue(
                    'stream',
                    $cachedStreamData[$streamUid]->streamIndex,
                    PDO::PARAM_INT,
                );
                $insertEventStatement->bindValue(
                    'number',
                    $actualNumber,
                    PDO::PARAM_INT,
                );
                $insertEventStatement->bindValue(
                    'type',
                    $event->eventType()->toString(),
                    PDO::PARAM_STR,
                );
                $insertEventStatement->bindValue(
                    'data',
                    $event->eventData()->toString(),
                    PDO::PARAM_STR,
                );
                $insertEventStatement->execute();
                $isFirstEvent = false;
                continue;
            }

            // try to roll back written events in case an outer transaction is active
            foreach ($cachedStreamData as $streamData) {
                $rollbackEventsStatement = $this->pdo->prepare(
                    'DELETE FROM
                        `' . $this->eventTable . '`
                    WHERE
                        `stream` = :stream AND
                        `number` > :number',
                );
                $rollbackEventsStatement->bindValue(
                    'stream',
                    $streamData->streamIndex,
                    PDO::PARAM_INT,
                );
                $rollbackEventsStatement->bindValue(
                    'number',
                    $streamData->initialNumber,
                    PDO::PARAM_INT,
                );
                $rollbackEventsStatement->execute();
            }

            if ($isFirstEvent && $expectedNumber > $actualNumber) {
                $msg = sprintf(
                    'Event number %1$d already present for stream "%2$s".',
                    $actualNumber,
                    $event->streamUid(),
                );
                throw new EventStoreConcurrencyException($msg);
            }

            $msg = sprintf(
                'Expected next event number to be %1$d, but is %2$d.',
                $expectedNumber,
                $actualNumber,
            );
            throw new EventStoreRuntimeException($msg);
        }
    }

    /**
     * @param StreamUid $uid
     * @param bool $createOnDemand
     * @return int
     * @throws PDOException
     * @no-named-arguments
     */
    private function streamIndex(StreamUid $uid, bool $createOnDemand): int
    {
        $statement = $this->pdo->prepare(
            'SELECT
                `stream`
            FROM
                `' . $this->uidTable . '`
            WHERE
                `uid` = :uid
            LIMIT
                1',
        );
        $statement->bindValue('uid', $uid->toString(), PDO::PARAM_STR);
        $statement->execute();

        $rows = $statement->fetchAll(PDO::FETCH_OBJ);
        assert($rows !== false);
        $rows[] = (object)['stream' => 0];
        /** @psalm-var non-empty-list<object{stream:int}> $rows */
        $streamIndex = (int)array_shift($rows)->stream;

        if ($streamIndex < 1 && $createOnDemand) {
            $createStreamStatement = $this->pdo->prepare(
                'INSERT INTO
                    `' . $this->uidTable . '`
                (
                    `uid`
                ) VALUES (
                    :uid
                )',
            );
            $createStreamStatement->bindValue('uid', $uid->toString(), PDO::PARAM_STR);
            $createStreamStatement->execute();
            $streamIndex = $this->streamIndex($uid, $createOnDemand);
        }

        return $streamIndex;
    }

    /**
     * @param int $streamIndex
     * @return int
     * @throws PDOException
     * @no-named-arguments
     */
    private function maxNumber(int $streamIndex): int
    {
        $statement = $this->pdo->prepare(
            'SELECT
                MAX(`number`) `max`
            FROM
                `' . $this->eventTable . '`
            WHERE
                `stream` = :stream
            LIMIT
                1',
        );
        $statement->bindValue('stream', $streamIndex, PDO::PARAM_INT);
        $statement->execute();

        $rows = $statement->fetchAll(PDO::FETCH_OBJ);
        assert($rows !== false);
        $rows[] = (object)['max' => 0];
        /** @psalm-var non-empty-list<object{max:int}> $rows */
        return (int)array_shift($rows)->max;
    }

    /**
     * @return int|null
     * @throws PDOException
     */
    private function setPdoErrorMode(): ?int
    {
        $oldErrorMode = $this->pdo->getAttribute(PDO::ATTR_ERRMODE);
        assert(is_int($oldErrorMode));
        if ($oldErrorMode === PDO::ERRMODE_EXCEPTION) {
            return null;
        }
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $oldErrorMode;
    }

    /**
     * @param int|null $errorMode
     * @no-named-arguments
     */
    private function restorePdoErrorMode(?int $errorMode): void
    {
        if ($errorMode === null) {
            return;
        }
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, $errorMode);
    }

    /**
     * @param EventReference $reference
     * @return int
     * @throws EventStoreRuntimeExceptionInterface
     * @no-named-arguments
     */
    private function resolveEventReference(EventReference $reference): int
    {
        $id = 0;
        if ((bool)preg_match('=^[1-9][0-9]*$=', $reference->toString())) {
            $id = (int)$reference->toString();
        }

        $statement = $this->pdo->prepare(
            'SELECT
                `id`
            FROM
                `' . $this->eventTable . '`
            WHERE
                `id` = :id
            LIMIT
                1',
        );
        $statement->bindValue('id', $id, PDO::PARAM_INT);
        $statement->execute();

        $rows = $statement->fetchAll(PDO::FETCH_OBJ);
        assert($rows !== false);
        $rows[] = (object)['id' => 0];
        /** @psalm-var non-empty-list<object{id:int}> $rows */
        $id = (int)array_shift($rows)->id;

        if ($id < 1) {
            $msg = sprintf(
                'Invalid event reference: %1$s',
                $reference->toString(),
            );
            throw new EventStoreRuntimeException($msg);
        }

        return $id;
    }
}
