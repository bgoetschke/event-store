<?php

declare(strict_types=1);

require __DIR__ . '/../vendor/autoload.php';
$pdo = new PDO('sqlite::memory:');
$createTablesSql = file_get_contents(__DIR__ . '/create_tables_sqlite.sql');
assert($createTablesSql !== false);
$pdo->exec($createTablesSql);
$eventStore = new \BjoernGoetschke\EventStore\PdoEventStore($pdo, 'UID_TABLE', 'EVENT_TABLE');

// the following code is the basic usage example from README.md

/** @var \BjoernGoetschke\EventStore\EventStoreInterface $eventStore */
// `$position` will be `null` because the stream does not exist yet
$position = $eventStore->position(new \BjoernGoetschke\EventStore\StreamUid('SomeUid'));
// write two events to the event store, in this case also to two different streams - each call to
// the write method will write either all the provided events, or none at all
$eventStore->write(
    [
        \BjoernGoetschke\EventStore\Event\StreamEvent::fromBasicTypes('SomeUid', 1, 'SomeType1', 'SomeData1'),
        \BjoernGoetschke\EventStore\Event\StreamEvent::fromBasicTypes('OtherUid', 1, 'OtherType1', 'OtherData1'),
    ],
);
// `$position` will be an `BjoernGoetschke\EventStore\EventNumber` object with the event number 1
// because now there is one event in the event stream
$position = $eventStore->position(new \BjoernGoetschke\EventStore\StreamUid('SomeUid'));
// `$streamEvents` will be an `BjoernGoetschke\EventStore\Stream\EventStreamInterface` object that will return
// all events of the `SomeUid` stream in sequential order (in this case the one event that has been stored before)
$streamEvents = $eventStore->streamEvents(new \BjoernGoetschke\EventStore\StreamUid('SomeUid'));
while (null !== $event = $streamEvents->next()) {
    echo $event->eventData() . PHP_EOL; // SomeData1
}
// `$allEvents` will be an `BjoernGoetschke\EventStore\Stream\EventStreamInterface` object that will return
// all events of the event store in sequential order (in this case the two events that has been stored before)
$allEvents = $eventStore->allEvents();
while (null !== $event = $allEvents->next()) {
    echo $event->eventData() . PHP_EOL; // SomeData1, OtherData1
}
