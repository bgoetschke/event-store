#!/usr/bin/env sh

set -e

exec ./scripts/tools.sh vendor/bin/phpcs --cache="phpcs-result.cache" "$@"
