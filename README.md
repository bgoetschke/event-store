# Event store library

This library provides the interface `BjoernGoetschke\EventStore\EventStoreInterface` that defines an abstraction layer
to a storage backend that can store sequential application specific events for an arbitrary number of separate
event streams.

The intended use case for this library is as backend for an event sourced application.

Currently, the only implementation is the class `BjoernGoetschke\EventStore\PdoEventStore` which uses a PDO database
connection as storage backend. It is tested with SQLite and MySQL and example SQL script files to create the
necessary database tables can be found in the `contrib` folder.

Additional features like event hooks and projections are intentionally left out to keep the scope of the
library small and focused on just the storage and retrieval of events.

## Basic usage

    /** @var \BjoernGoetschke\EventStore\EventStoreInterface $eventStore */
    // `$position` will be `null` because the stream does not exist yet
    $position = $eventStore->position(new \BjoernGoetschke\EventStore\StreamUid('SomeUid'));
    // write two events to the event store, in this case also to two different streams - each call to
    // the write method will write either all the provided events, or none at all
    $eventStore->write(
        [
            \BjoernGoetschke\EventStore\Event\StreamEvent::fromBasicTypes('SomeUid', 1, 'SomeType1', 'SomeData1'),
            \BjoernGoetschke\EventStore\Event\StreamEvent::fromBasicTypes('OtherUid', 1, 'OtherType1', 'OtherData1')
        ]
    );
    // `$position` will be an `BjoernGoetschke\EventStore\EventNumber` object with the event number 1
    // because now there is one event in the event stream
    $position = $eventStore->position(new \BjoernGoetschke\EventStore\StreamUid('SomeUid'));
    // `$streamEvents` will be an `BjoernGoetschke\EventStore\Stream\EventStreamInterface` object that will return
    // all events of the `SomeUid` stream in sequential order (in this case the one event that has been stored before)
    $streamEvents = $eventStore->streamEvents(new \BjoernGoetschke\EventStore\StreamUid('SomeUid'));
    while (null !== $event = $streamEvents->next()) {
        echo $event->eventData() . PHP_EOL; // SomeData1
    }
    // `$allEvents` will be an `BjoernGoetschke\EventStore\Stream\EventStreamInterface` object that will return
    // all events of the event store in sequential order (in this case the two events that has been stored before)
    $allEvents = $eventStore->allEvents();
    while (null !== $event = $allEvents->next()) {
        echo $event->eventData() . PHP_EOL; // SomeData1, OtherData1
    }

## Installation

The library is available via Composer:

    composer require bjoern-goetschke/event-store:^2.0

## Versioning

Releases will be numbered with the following format using semantic versioning:

`<major>.<minor>.<patch>`

And constructed with the following guidelines:

* Breaking backwards compatibility bumps the major
* New additions without breaking backwards compatibility bumps the minor
* Bug fixes and misc changes bump the patch

For more information on semantic versioning, please visit http://semver.org/.

## LICENSE

The library is released under the BSD-2-Clause license. You can find a copy of this license in LICENSE.txt.

## API usage and backwards compatibility

Information about the intended usage of interfaces, classes, methods, etc. is specified with the `@api` tag.

If an element does not contain the `@api` tag it should be considered internal and usage may break at any time.

One exception to this rule are special elements like constructors, destructors or other hook methods that are defined
by the programming language. These elements will not have their own `@api` tag but can be considered as if they have
the same `@api` tag as the class or whatever other element they belong to.

The library DOES NOT provide a backwards-compatibility promise for parameter names. Methods will have the
`@no-named-arguments` tag to help static analysis tools in detecting and warning about using the library with
named arguments, but in case it is missing somewhere that does not mean that a backwards-compatibility promise
is given for that specific method.

### `@api usage`

* Classes
    * Create new instances of the class
        * may break on `major`-releases
    * Extending the class and adding a new constructor
        * may break on `major`-releases
    * Extending the class and adding new methods
        * may break at any time, but `minor`-releases should be ok most of the time
          (will break if a non-private method has been added to the base class that was also declared
          in the extending class)
* Methods
    * Calling the method
        * may break on `major`-releases
    * Overriding the method (extending the class and declaring a method with the same name) and eventually
      adding additional optional arguments
        * may break at any time, but `minor`-releases should be ok most of the time
          (will break if an optional argument has been added to the method in the base-class)
* Interfaces
    * Using the interface in type-hints (require an instance of the interface as argument)
        * may break on `major`-releases
    * Calling methods of the interface
        * may break on `major`-releases
    * Implementing the interface
        * may break at any time, but `minor`-releases should be ok most of the time
          (will break if new methods have been added to the interface)
    * Extending the interface
        * may break at any time, but `minor`-releases should be ok most of the time
          (will break if a method has been added to the base interface that was also declared
          in the extending interface)

### `@api extend`

* Classes
    * Create new instances of the class
        * may break on `major`-releases
    * Extending the class and adding a new constructor
        * may break on `major`-releases
    * Extending the class and adding new methods
        * may break on `minor`-releases, but it should be ok most of the time and may only break on `major`-releases
          (will break if a non-private method has been added to the base class that was also declared in the
          extending class)
* Methods
    * Calling the method
        * may break on `major`-releases
    * Overriding the method (extending the class and declaring a method with the same name) and eventually
      adding additional optional arguments
        * may break on `major`-releases
* Interfaces
    * Using the interface in type-hints (require an instance of the interface as argument)
        * may break on `major`-releases
    * Calling methods of the interface
        * may break on `major`-releases
    * Implementing the interface
        * may break on `major`-releases
    * Extending the interface
        * may break on `major`-releases

### `@api stable`

* Everything that is marked as stable may only break on `major`-releases, this means that except some minor internal
  changes or bugfixes the code will just never change at all

### `@api internal`

* Everything that is marked as internal may break at any time, but `patch`-releases should be ok most of the time
